package biom4st3r.asmr_compiler;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import biom4st3r.asmr_compiler.Compiler.Tree;
import biom4st3r.asmr_compiler.asm_accessed.InjectTransformerDesc;
import biom4st3r.asmr_compiler.templatetransformers.InjectTransformerBase;
import biom4st3r.asmr_compiler.templatetransformers.TransformerGenerator;

import org.junit.jupiter.api.Test;
import org.quiltmc.asmr.processor.AsmrPlatform;
import org.quiltmc.asmr.processor.AsmrProcessor;
import org.quiltmc.asmr.processor.AsmrTransformer;

public class Tests {
    public static byte[] findClassBytes(Class<?> clazz) {
		Path sourceLocation;
		try {
			sourceLocation = Paths.get(clazz.getProtectionDomain().getCodeSource().getLocation().toURI());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			throw new NoClassDefFoundError(clazz.getName());
		}

		byte[] bytes;
		try {
			if (Files.isDirectory(sourceLocation)) {
				Path classFile = sourceLocation.resolve(clazz.getName().replace('.', File.separatorChar) + ".class");
				bytes = Files.readAllBytes(classFile);
			} else {
				try (FileSystem fs = FileSystems.newFileSystem(sourceLocation, (ClassLoader) null);) {
					Path jarEntry = fs.getPath(clazz.getName().replace('.', '/') + ".class");
					bytes = Files.readAllBytes(jarEntry);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new NoClassDefFoundError(clazz.getName());
		}

		return bytes;
	}
    
    private static AsmrProcessor getProcessor(AsmrPlatform plateform) {
        System.gc();
        AsmrProcessor processor = new AsmrProcessor(plateform);
        try {
            String className = "net/minecraft/server/PlayerManager";
            processor.addClass(className, plateform.getClassBytecode(className));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.gc();
        
        return processor;
    }

    static AsmrPlatform plateform = new AsmrPlatform() {
        @Override
        public byte[] getClassBytecode(String arg0) throws ClassNotFoundException {
            return findClassBytes(Class.forName(arg0.replace("/", ".")));
        }
    };


    static InjectTransformerDesc getTransformer(InputStream stream, String name) {
        String[] insns = null;
        try {
            insns = Compiler.getInsn(stream, name);
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<Tree> trees = Compiler.growTrees(insns);
        InjectTransformerDesc transformerDescription = Compiler.InjectCompiler.buildInjectTransformer(trees);
        return transformerDescription;
    }

    public void testInjector(String filename) {
        AsmrPlatform plateform = new AsmrPlatform() {
            @Override
            public byte[] getClassBytecode(String arg0) throws ClassNotFoundException {
                return findClassBytes(Class.forName(arg0.replace("/", ".")));
            }
        };
        AsmrProcessor processor = getProcessor(plateform);
        
        InputStream stream = getClass().getClassLoader().getResourceAsStream(filename);
        InjectTransformerDesc transform = getTransformer(stream, filename);
        String className = "biom4st3r/asmtransformer/InjectTransformer" + transform.index;

        // Class<? extends AsmrTransformer> clazz = InjectTransformerBase.generateTransformerClass(className, transform, processor);
        Class<? extends AsmrTransformer> clazz = TransformerGenerator.getTransformerClass(className, InjectTransformerBase.class, transform, InjectTransformerDesc.class, processor);
        processor.addTransformer(clazz);
    }

    public void printSuccess() {
        System.out.println("Success: " + new Exception().getStackTrace()[1].toString());
    }
    public void printFaliure() {
        System.out.println("Failure: " + new Exception().getStackTrace()[1].toString());
    }

    @Test
    public void test_standard_file() {
        try {
            testInjector("test0.asmr");
            printSuccess();
        } catch(Throwable t) {
            printFaliure();
            throw new RuntimeException(t);
        }
    }

    @Test
    public void test_empty_file() {
        // Test empty config
        try {
            testInjector("test_empty.asmr");
            printFaliure();
            throw new RuntimeException();
        } catch(Biom4st3rCompilerError t) {
            printSuccess();
            // t.printStackTrace();
        }
    }

    @Test
    public void test_file_doesnt_exist() {
        try {
            testInjector("test-1.asmr");
            printFaliure();
            throw new RuntimeException();
        } catch(Biom4st3rCompilerError t) {
            printSuccess();
            // t.printStackTrace();
        }
    }

    @Test
    public void test_file_bad_header() {
        try {
            testInjector("test_bad_header.asmr");
            printFaliure();
            throw new RuntimeException();
        } catch(Biom4st3rCompilerError t) {
            printSuccess();
            // t.printStackTrace();
        }
    }

    @Test
    public void test_malformed_methodTarget0() {
        try {
            testInjector("test_malformed_methodTarget0.asmr");
            printFaliure();
            throw new RuntimeException();
        } catch(Biom4st3rCompilerError t) {
            printSuccess();
            // t.printStackTrace();
        }
    }

    @Test
    public void test_malformed_methodTarget1() {
        try {
            testInjector("test_malformed_methodTarget1.asmr");
            printFaliure();
            throw new RuntimeException();
        } catch(Biom4st3rCompilerError t) {
            printSuccess();
            // t.printStackTrace();
        }
    }
    @Test
    public void test_lvstore_in_match() {
        try {
            testInjector("test_bad_match0.asmr");
            printFaliure();
            throw new RuntimeException();
        } catch(Biom4st3rCompilerError t) {
            printSuccess();
            // t.printStackTrace();
        }
    }
    @Test
    public void test_lvload_in_match() {
        try {
            testInjector("test_bad_match1.asmr");
            printFaliure();
            throw new RuntimeException();
        } catch(Biom4st3rCompilerError t) {
            printSuccess();
            // t.printStackTrace();
        }
    }    
    @Test
    public void test_lvstore_illegal_in_match() {
        try {
            testInjector("test_bad_match3.asmr");
            printFaliure();
            throw new RuntimeException();
        } catch(Biom4st3rCompilerError t) {
            printSuccess();
            // t.printStackTrace();
        }
    }
    @Test
    public void test_invalid_match_position() {
        try {
            testInjector("test_bad_position.asmr");
            printFaliure();
            throw new RuntimeException();
        } catch(Biom4st3rCompilerError t) {
            printSuccess();
        }
    }
}
