package biom4st3r.asmr_compiler;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import biom4st3r.asmr_compiler.util.Lists;
import biom4st3r.asmr_compiler.util.Maps;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.IntInsnNode;
import org.objectweb.asm.tree.LdcInsnNode;

public class Asm {
    public static enum Stack {
            LOAD,
            STORE
    }
    public static interface TriConsumer<A,B,C> {
        void accept(A a, B b, C c);
    }

    public static class Primitive {
        private static Map<Class<?>,Primitive> primitives = Maps.newHashMap();
        public final Class<?> clazz;
        private final Consumer<MethodVisitor> doReturn;
        private final BiConsumer<Stack,MethodVisitor> touchArray;
        private final TriConsumer<Stack,MethodVisitor,Integer> touchVar;
        private final Consumer<MethodVisitor> loadDefaultValue;
        private final int arrayType;
        private final int loadVarInsn;
        private final int storeVarInsn;

        public void _return(MethodVisitor mv) {
            this.doReturn.accept(mv);
        }
        public void touchArray(Stack stack, MethodVisitor mv) {
            this.touchArray.accept(stack, mv);
        }
        public void touchVar(Stack stack, MethodVisitor mv, int i) {
            this.touchVar.accept(stack,mv,i);
        }
        public int arrayType() {
            return this.arrayType;
        }
        public void loadDefaultValue(MethodVisitor mv) {
            this.loadDefaultValue.accept(mv);
        } 
        public int getLoadInsn() {
            return this.loadVarInsn;
        }
        public int getStoreInsn() {
            return this.storeVarInsn;
        }

        private Primitive(Class<?> clazz, Consumer<MethodVisitor> doReturn, BiConsumer<Stack,MethodVisitor> touchArray, TriConsumer<Stack,MethodVisitor,Integer> touchVar, int arrayOpcode, Consumer<MethodVisitor> loadDefaultValue, int loadVarInsn, int storeVarInsn) {
            primitives.put(clazz, this);
            this.clazz = clazz;
            this.doReturn = doReturn;
            this.touchArray = touchArray;
            this.touchVar = touchVar;
            this.arrayType = arrayOpcode;
            this.loadDefaultValue = loadDefaultValue;
            this.loadVarInsn = loadVarInsn;
            this.storeVarInsn = storeVarInsn;
        }

        public static Primitive get(Class<?> clazz) {
            return primitives.getOrDefault(clazz, OBJECT);
        }

        public static Primitive CHAR = new Primitive(
            char.class, 
            (mv)->mv.visitInsn(Opcodes.IRETURN), 
            (stack,mv)->mv.visitInsn(stack == Stack.LOAD ? Opcodes.CALOAD : Opcodes.CASTORE), 
            (stack,mv,i)->mv.visitVarInsn(stack == Stack.LOAD ? Opcodes.ILOAD : Opcodes.ISTORE, i),
            Opcodes.T_CHAR,
            (mv)->mv.visitInsn(Opcodes.ICONST_M1),
            Opcodes.ILOAD,
            Opcodes.ISTORE
        );
        public static Primitive BYTE = new Primitive(
            byte.class, 
            (mv)->mv.visitInsn(Opcodes.IRETURN), 
            (stack,mv)->mv.visitInsn(stack == Stack.LOAD ? Opcodes.BALOAD : Opcodes.BASTORE), 
            (stack,mv,i)->mv.visitVarInsn(stack == Stack.LOAD ? Opcodes.ILOAD : Opcodes.ISTORE, i),
            Opcodes.T_BYTE,
            (mv)->mv.visitInsn(Opcodes.ICONST_M1),
            Opcodes.ILOAD,
            Opcodes.ISTORE
        );
        public static Primitive SHORT = new Primitive(
            short.class, 
            (mv)->mv.visitInsn(Opcodes.IRETURN), 
            (stack,mv)->mv.visitInsn(stack == Stack.LOAD ? Opcodes.SALOAD : Opcodes.SASTORE), 
            (stack,mv,i)->mv.visitVarInsn(stack == Stack.LOAD ? Opcodes.ILOAD : Opcodes.ISTORE, i),
            Opcodes.T_SHORT,
            (mv)->mv.visitInsn(Opcodes.ICONST_M1),
            Opcodes.ILOAD,
            Opcodes.ISTORE
        );
        public static Primitive INT = new Primitive(
            int.class, 
            (mv)->mv.visitInsn(Opcodes.IRETURN), 
            (stack,mv)->mv.visitInsn(stack == Stack.LOAD ? Opcodes.IALOAD : Opcodes.IASTORE), 
            (stack,mv,i)->mv.visitVarInsn(stack == Stack.LOAD ? Opcodes.ILOAD : Opcodes.ISTORE, i),
            Opcodes.T_INT,
            (mv)->mv.visitInsn(Opcodes.ICONST_M1),
            Opcodes.ILOAD,
            Opcodes.ISTORE
        );
        public static Primitive LONG = new Primitive(
            long.class, 
            (mv)->mv.visitInsn(Opcodes.LRETURN), 
            (stack,mv)->mv.visitInsn(stack == Stack.LOAD ? Opcodes.LALOAD : Opcodes.LASTORE), 
            (stack,mv,i)->mv.visitVarInsn(stack == Stack.LOAD ? Opcodes.LLOAD : Opcodes.LSTORE, i),
            Opcodes.T_LONG,
            (mv)->mv.visitInsn(Opcodes.LCONST_0),
            Opcodes.LLOAD,
            Opcodes.LSTORE
        );
        public static Primitive FLOAT = new Primitive(
            float.class, 
            (mv)->mv.visitInsn(Opcodes.FRETURN), 
            (stack,mv)->mv.visitInsn(stack == Stack.LOAD ? Opcodes.FALOAD : Opcodes.FASTORE), 
            (stack,mv,i)->mv.visitVarInsn(stack == Stack.LOAD ? Opcodes.FLOAD : Opcodes.FSTORE, i),
            Opcodes.T_FLOAT,
            (mv)->mv.visitInsn(Opcodes.FCONST_0),
            Opcodes.FLOAD,
            Opcodes.FSTORE
        );
        public static Primitive DOUBLE = new Primitive(
            double.class, 
            (mv)->mv.visitInsn(Opcodes.DRETURN), 
            (stack,mv)->mv.visitInsn(stack == Stack.LOAD ? Opcodes.DALOAD : Opcodes.DASTORE), 
            (stack,mv,i)->mv.visitVarInsn(stack == Stack.LOAD ? Opcodes.DLOAD : Opcodes.DSTORE, i),
            Opcodes.T_DOUBLE,
            (mv)->mv.visitInsn(Opcodes.DCONST_0),
            Opcodes.DLOAD,
            Opcodes.DSTORE
        );
        public static Primitive BOOL = new Primitive(
            boolean.class, 
            (mv)->mv.visitInsn(Opcodes.IRETURN), 
            (stack,mv)->mv.visitInsn(stack == Stack.LOAD ? Opcodes.BALOAD : Opcodes.BASTORE), 
            (stack,mv,i)->mv.visitVarInsn(stack == Stack.LOAD ? Opcodes.ILOAD : Opcodes.ISTORE, i),
            Opcodes.T_BOOLEAN,
            (mv)->mv.visitInsn(Opcodes.ICONST_0),
            Opcodes.ILOAD,
            Opcodes.ISTORE
        );
        public static Primitive OBJECT = new Primitive(
            Object.class, 
            (mv)->mv.visitInsn(Opcodes.ARETURN), 
            (stack,mv)->mv.visitInsn(stack == Stack.LOAD ? Opcodes.AALOAD : Opcodes.AASTORE), 
            (stack,mv,i)->mv.visitVarInsn(stack == Stack.LOAD ? Opcodes.ALOAD : Opcodes.ASTORE, i),
            -1,
            (mv)->mv.visitInsn(Opcodes.ACONST_NULL),
            Opcodes.ALOAD,
            Opcodes.ASTORE
        );
    }

    public static Primitive get(Class<?> clazz) {
        return Primitive.get(clazz);
    }

    public static Primitive get(Type desc) {
        for(Entry<Class<?>, Primitive> entry : Primitive.primitives.entrySet()) {
            if(desc.getDescriptor().equals(Type.getDescriptor(entry.getKey()))) {
                return entry.getValue();
            }
        }
        return Primitive.OBJECT;
    }

    public static void pushLong(MethodVisitor mv, long val) {
        if(val == 0) {
            mv.visitInsn (Opcodes.LCONST_0);
        } else if(val == 1) {
            mv.visitInsn (Opcodes.LCONST_1);
        } else if(val < Integer.MIN_VALUE || val > Integer.MAX_VALUE) {
            mv.visitLdcInsn(val);
        }
        else {
            pushInt(mv, (int) val);
        }
    }

    public static void pushInt(MethodVisitor mv, int val) {
        if(val == -1) {
            mv.visitInsn(Opcodes.ICONST_M1);
        }
        else if (val == 0) {
            mv.visitInsn(Opcodes.ICONST_0);
        }
        else if (val == 1) {
            mv.visitInsn(Opcodes.ICONST_1);
        }
        else if (val == 2) {
            mv.visitInsn(Opcodes.ICONST_2);
        }
        else if (val == 3) {
            mv.visitInsn(Opcodes.ICONST_3);
        }
        else if (val == 4) {
            mv.visitInsn(Opcodes.ICONST_4);
        }
        else if (val == 5) {
            mv.visitInsn(Opcodes.ICONST_5);
        }
        else if (val <= Byte.MAX_VALUE && val >= Byte.MIN_VALUE) {
            mv.visitIntInsn(Opcodes.BIPUSH, val);
        }
        else if (val <= Short.MAX_VALUE && val >= Short.MIN_VALUE) {
            mv.visitIntInsn(Opcodes.SIPUSH, val);
        } else {
            mv.visitLdcInsn(val);
        }
    }

    
    public static String qualifyName(String s) {
        return s.replace('.', '/');
    }
    
    static List<Class<?>> prims = Lists.newArrayList(int.class,long.class,float.class,double.class,char.class,byte.class,short.class,boolean.class);
    
    public static Collection<Class<?>> getPrims() {
        return prims;
    }

    public static boolean isPrimitive(Class<?> clazz) {
        return prims.contains(clazz);
    }

    public static int getArrayOpcode(Class<?> clazz) {
        return Type.getDescriptor(clazz).startsWith("L") ? Opcodes.ANEWARRAY : Opcodes.NEWARRAY;
    }

    /**
     * Leaves a initialized Object on the stack
     * @param mv
     * @param loadVars
     * @param clazzName
     * @param desc
     */
    public static void ctor(MethodVisitor mv, Consumer<MethodVisitor> loadVars,String clazzName, String desc) {
        mv.visitTypeInsn(Opcodes.NEW, qualifyName(clazzName));
        mv.visitInsn(Opcodes.DUP);
        loadVars.accept(mv);
        mv.visitMethodInsn(Opcodes.INVOKESPECIAL, qualifyName(clazzName), "<init>", desc, false);
    }

    public static final Number insnToNumber(AbstractInsnNode node) {
        int i = node.getOpcode();
        switch(i) {
            case Opcodes.ICONST_M1:
            return -1;
            case Opcodes.ICONST_0:
            return 0;
            case Opcodes.ICONST_1:
            return 1;
            case Opcodes.ICONST_2:
            return 2;
            case Opcodes.ICONST_3:
            return 3;
            case Opcodes.ICONST_4:
            return 4;
            case Opcodes.ICONST_5:
            return 5;
            case Opcodes.LCONST_0:
            return 0L;
            case Opcodes.LCONST_1:
            return 1L;
            case Opcodes.DCONST_0:
            return 0.0D;
            case Opcodes.DCONST_1:
            return 1.0D;
            case Opcodes.FCONST_0:
            return 0.0F;
            case Opcodes.FCONST_1:
            return 1.0F;
            case Opcodes.FCONST_2:
            return 2.0F;
            case Opcodes.BIPUSH:
            case Opcodes.SIPUSH:
            return ((IntInsnNode)node).operand;
            case Opcodes.LDC:
            return (Number) ((LdcInsnNode)node).cst;
        }
        return 0xFFFFFFFFFFFFFFFFL;
    }
}
