package biom4st3r.asmr_compiler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.util.CheckClassAdapter;
import org.objectweb.asm.util.TraceClassVisitor;
import org.quiltmc.asmr.processor.AsmrClassWriter;
import org.quiltmc.asmr.processor.AsmrPlatform;
import org.quiltmc.asmr.processor.AsmrProcessor;
import org.quiltmc.asmr.processor.tree.member.AsmrClassNode;

import biom4st3r.asmr_compiler.Compiler.Tree;
import biom4st3r.asmr_compiler.asm_accessed.InjectTransformerDesc;
import biom4st3r.asmr_compiler.templatetransformers.InjectTransformerBase;
import biom4st3r.asmr_compiler.templatetransformers.TransformerGenerator;
import biom4st3r.asmr_compiler.util.AsmrUtil;

public class Main {

    public static byte[] findClassBytes(Class<?> clazz) {
		Path sourceLocation;
		try {
			sourceLocation = Paths.get(clazz.getProtectionDomain().getCodeSource().getLocation().toURI());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			throw new NoClassDefFoundError(clazz.getName());
		}

		byte[] bytes;
		try {
			if (Files.isDirectory(sourceLocation)) {
				Path classFile = sourceLocation.resolve(clazz.getName().replace('.', File.separatorChar) + ".class");
				bytes = Files.readAllBytes(classFile);
			} else {
				try (FileSystem fs = FileSystems.newFileSystem(sourceLocation, (ClassLoader) null);) {
					Path jarEntry = fs.getPath(clazz.getName().replace('.', '/') + ".class");
					bytes = Files.readAllBytes(jarEntry);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new NoClassDefFoundError(clazz.getName());
		}

		return bytes;
	}

    private static AsmrProcessor getProcessor(AsmrPlatform plateform) {
        System.gc();
        AsmrProcessor processor = new AsmrProcessor(plateform);
        try {
            String className = "net/minecraft/server/PlayerManager";
            processor.addClass(className, plateform.getClassBytecode(className));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.gc();
        
        return processor;
    }

    public static void main(String[] args) {
        AsmrPlatform plateform = new AsmrPlatform() {
            @Override
            public byte[] getClassBytecode(String arg0) throws ClassNotFoundException {
                return findClassBytes(Class.forName(arg0.replace("/", ".")));
            }
        };
        AsmrProcessor processor = getProcessor(plateform);
        
        processor.addTransformer(Compiler.parseTransformer(processor, new File("objectReplacer.asmr")));
        // processor.addTransformer(Compiler.parseTransformer(processor, new File("bs.asmr")));
        processor.process();
        
        try {
            AsmrClassWriter cw = new AsmrClassWriter(processor);
            ClassVisitor cv = new ClassVisitor(Opcodes.ASM8,cw){
                @Override
                public void visit(int version, int access, String name, String signature, String superName,
                        String[] interfaces) {
                    super.visit(Opcodes.V1_8, access, name, signature, superName, interfaces);
                }
            };
            AsmrClassNode acn = processor.findClassImmediately("net/minecraft/server/PlayerManager");
            
            acn.accept(new CheckClassAdapter(cv, false));
            File f = new File("bsasm" + System.nanoTime() + ".class");
            FileOutputStream stream = new FileOutputStream(f);
            stream.write(cw.toByteArray());
            stream.close(); 
            
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            testTransomfation(processor);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    private static void testTransomfation(AsmrProcessor processor) throws Throwable {
        Class clazz = AsmrUtil.defineClass(processor, processor.findClassImmediately("net/minecraft/server/PlayerManager"));
        Object o = clazz.getDeclaredConstructors()[0].newInstance();
        Method testRandom = clazz.getDeclaredMethod("randomTest", int.class);
        System.out.println(testRandom.invoke(o, 99));
    }

    @SuppressWarnings({"rawtypes","unchecked"})
    /**
     * Main.Transformer is a template used by 
     * @param args
     */
    public static void Oldmain(String[] args) {
        File file = new File("bs.asmr");
        AsmrPlatform plateform = new AsmrPlatform() {
            @Override
            public byte[] getClassBytecode(String arg0) throws ClassNotFoundException {
                return findClassBytes(Class.forName(arg0.replace("/", ".")));
            }
        };
        AsmrProcessor processor = getProcessor(plateform);
        try {
            String[] insns = Compiler.getInsns(file);
            List<Tree> trees = Compiler.growTrees(insns);
            InjectTransformerDesc transformerDescription = Compiler.InjectCompiler.buildInjectTransformer(trees);

            // Make Transformer
            String className = "biom4st3r/asmtransformer/InjectTransformer"+transformerDescription.index;
            Class clazz = TransformerGenerator.getTransformerClass(className, InjectTransformerBase.class, transformerDescription, InjectTransformerDesc.class, processor);
            // Class clazz = InjectTransformerBase.generateTransformerClass(className, transformerDescription, processor);
            processor.addTransformer(clazz);
            processor.process();

            // Get Transformed Class Bytecode
            AsmrClassWriter cw = new AsmrClassWriter(processor);
            CheckClassAdapter cca = new CheckClassAdapter(cw, false);
            PrintWriter pw = null;
            try { 
                pw = new PrintWriter(new File("bsasmr.txt"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            TraceClassVisitor tcv = new TraceClassVisitor(cca, pw);

            AsmrClassNode acn = processor.findClassImmediately("net/minecraft/server/PlayerManager");
            acn.accept(tcv);
            File f = new File("bsasmr" + System.nanoTime() + ".class");
            FileOutputStream stream = new FileOutputStream(f);
            stream.write(cw.toByteArray());
            stream.close();

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
}
