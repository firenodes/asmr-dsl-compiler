package biom4st3r.asmr_compiler;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Supplier;

import biom4st3r.asmr_compiler.insns_v2.FieldInsn;
import biom4st3r.asmr_compiler.insns_v2.Insn;
import biom4st3r.asmr_compiler.insns_v2.IntInsn;
import biom4st3r.asmr_compiler.insns_v2.LVInsn;
import biom4st3r.asmr_compiler.insns_v2.LdcInsn;
import biom4st3r.asmr_compiler.insns_v2.MethodInsn;
import biom4st3r.asmr_compiler.insns_v2.NoOperandInsn;
import biom4st3r.asmr_compiler.insns_v2.TypeInsn;
import biom4st3r.asmr_compiler.insns_v2.VarInsn;
import biom4st3r.asmr_compiler.util.Lists;
import biom4st3r.asmr_compiler.util.Maps;

public class ConverterDoDad {

    public static Insn getNodeFromString(String[] s) {
        // Function<String, AbstractInsnNode> i = nameToInsnNode.get(s.split(" ")[0]);
        // if(i == null) {
        //     throw new Biom4st3rCompilerError("Instruction not found: %s", s);
        // }
        // try {
        //     return i.apply(s);
        // } catch(ArrayIndexOutOfBoundsException e) {
        //     throw new Biom4st3rCompilerError("Malformed Instruction: %s", s);
        // }
        return getInsn(s);
    }
/**
"ATHROW"
"MONITORENTER"
"MONITOREXIT"
"MULTIANEWARRAY" 
"INVOKEDYNAMIC"
"LOOKUPSWITCH"
"TABLESWITCH"
*/
    public static Insn getInsn(String[] split) {
        String s = String.join(" ", split);
        String opcode = split[0].toUpperCase();
        int iOpcode = nameToOpcodeInt.getOrDefault(opcode,-1);
        if(lvVarInsn.contains(opcode)) { // Load Local Variable. Search for specified variable type in LVT
            org.objectweb.asm.Type type;
            if(split[1].length() == 1) {
                type = org.objectweb.asm.Type.getType(split[1]);
            } else {
                type = org.objectweb.asm.Type.getObjectType(split[1]);
            }
            return new LVInsn(opcode, type);
        } else if(insnNode.contains(opcode)) {
            return new NoOperandInsn(iOpcode);
        } else if(varInsnNode.contains(opcode)) {
            if(split.length < 2) throw new Biom4st3rCompilerError("Invalid %s: %s", opcode, s);
            return new VarInsn(iOpcode, Integer.parseInt(split[1]));
        } else if(fieldInsnNode.contains(opcode)) {
            if(split.length < 4) throw new Biom4st3rCompilerError("Invalid %s: %s", opcode, s);
            String owner = split[1];
            String name = split[2];
            String desc = split[3];
            return new FieldInsn(iOpcode, owner, name, desc);
        } else if(opcode.equals("LDC")) {
            String number = split[1];
            try {
                return new LdcInsn(Integer.parseInt(number));
            } catch(Throwable t){}
            try {
                return new LdcInsn(Long.parseLong(number));
            } catch(Throwable t){}
            try {
                return new LdcInsn(Double.parseDouble(number));
            } catch(Throwable t){}
            try {
                return new LdcInsn(Float.parseFloat(number));
            } catch(Throwable t){}
            return new LdcInsn(number);
        } else if(methodInsnNode.contains(opcode)) {
            if(split.length < 4) throw new Biom4st3rCompilerError("Invalid %s: %s", opcode, s);
            String owner = split[1];
            String name = split[2];
            String desc = split[3];
            return new MethodInsn(iOpcode, owner, name, desc);
        } else if(typeInsnNode.contains(opcode)) {
            if(split.length < 2) throw new Biom4st3rCompilerError("Invalid %s: %s", opcode, s);
            return new TypeInsn(iOpcode, split[1]);
        } else if(intInsnNode.contains(opcode)) {
            if(split.length < 2) throw new Biom4st3rCompilerError("Invalid %s: %s", opcode, s);
            return new IntInsn(iOpcode, Integer.parseInt(split[1]));
        } else if(jump.contains(opcode)) {
            throw new Biom4st3rCompilerError("Jump not supported: %s", s);
        } else {
            throw new Biom4st3rCompilerError("Unsupported or invalid opcode: %s", s);
        }

    }
    public final static List<String> 
        insnNode = Lists.newArrayList("NOP", "ACONST_NULL", "ICONST_M1", "ICONST_0", "ICONST_1", "ICONST_2", "ICONST_3", "ICONST_4", "ICONST_5", "LCONST_0", "LCONST_1", "FCONST_0", "FCONST_1", "FCONST_2", "DCONST_0", "DCONST_1", "IALOAD", "LALOAD", "FALOAD", "DALOAD", "AALOAD", "BALOAD", "CALOAD", "SALOAD", "IASTORE", "LASTORE", "FASTORE", "DASTORE", "AASTORE", "BASTORE", "CASTORE", "SASTORE", "POP", "POP2", "DUP", "DUP_X1", "DUP_X2", "DUP2", "DUP2_X1", "DUP2_X2", "SWAP", "IADD", "LADD", "FADD", "DADD", "ISUB", "LSUB", "FSUB", "DSUB", "IMUL", "LMUL", "FMUL", "DMUL", "IDIV", "LDIV", "FDIV", "DDIV", "IREM", "LREM", "FREM", "DREM", "INEG", "LNEG", "FNEG", "DNEG", "ISHL", "LSHL", "ISHR", "LSHR", "IUSHR", "LUSHR", "IAND", "LAND", "IOR", "LOR", "IXOR", "LXOR", "IINC", "I2L", "I2F", "I2D", "L2I", "L2F", "L2D", "F2I", "F2L", "F2D", "D2I", "D2L", "D2F", "I2B", "I2C", "I2S", "LCMP", "FCMPL", "FCMPG", "DCMPL", "DCMPG", "IRETURN", "LRETURN", "FRETURN", "DRETURN", "ARETURN", "RETURN", "ARRAYLENGTH"),
        varInsnNode = Lists.newArrayList("ILOAD", "LLOAD", "FLOAD", "DLOAD", "ALOAD", "ISTORE", "LSTORE", "FSTORE", "DSTORE", "ASTORE"),
        fieldInsnNode = Lists.newArrayList("GETSTATIC","PUTSTATIC","GETFIELD","PUTFIELD"),
        methodInsnNode = Lists.newArrayList("INVOKEVIRTUAL", "INVOKESPECIAL", "INVOKESTATIC", "INVOKEINTERFACE"),
        typeInsnNode = Lists.newArrayList("NEW", "ANEWARRAY", "CHECKCAST", "INSTANCEOF"),
        intInsnNode = Lists.newArrayList("BIPUSH", "SIPUSH", "NEWARRAY"),
        // invokedynamic = Lists.newArrayList("INVOKEDYNAMIC"),
        // lookupSwitch = Lists.newArrayList("LOOKUPSWITCH"),
        // tableSwitch = Lists.newArrayList("TABLESWITCH"),
        jump = Lists.newArrayList("IFNE", "IFLT", "IFGE", "IFGT", "IFLE", "IF_ICMPEQ", "IF_ICMPNE", "IF_ICMPLT", "IF_ICMPGE", "IF_ICMPGT", "IF_ICMPLE", "IF_ACMPEQ", "IF_ACMPNE", "GOTO", "IFNULL", "IFNONNULL"),
        java7 = Lists.newArrayList("JSR", "RET"),
        lvVarInsn = Lists.newArrayList("LVLOAD","LVSTORE","LVLOAD_ILLEGAL","LVSTORE_ILLEGAL");
        ;
    
    public static final Map<String,Integer> nameToOpcodeInt = ((Supplier<Map<String,Integer>>)()-> {
        Map<String,Integer> map = Maps.newHashMap();
        map.put("NOP",0);
        map.put("ACONST_NULL",1);
        map.put("ICONST_M1",2);
        map.put("ICONST_0",3);
        map.put("ICONST_1",4);
        map.put("ICONST_2",5);
        map.put("ICONST_3",6);
        map.put("ICONST_4",7);
        map.put("ICONST_5",8);
        map.put("LCONST_0",9);
        map.put("LCONST_1",10);
        map.put("FCONST_0",11);
        map.put("FCONST_1",12);
        map.put("FCONST_2",13);
        map.put("DCONST_0",14);
        map.put("DCONST_1",15);
        map.put("BIPUSH",16);
        map.put("SIPUSH",17);
        map.put("LDC",18);
        map.put("ILOAD",21);
        map.put("LLOAD",22);
        map.put("FLOAD",23);
        map.put("DLOAD",24);
        map.put("ALOAD",25);
        map.put("IALOAD",46);
        map.put("LALOAD",47);
        map.put("FALOAD",48);
        map.put("DALOAD",49);
        map.put("AALOAD",50);
        map.put("BALOAD",51);
        map.put("CALOAD",52);
        map.put("SALOAD",53);
        map.put("ISTORE",54);
        map.put("LSTORE",55);
        map.put("FSTORE",56);
        map.put("DSTORE",57);
        map.put("ASTORE",58);
        map.put("IASTORE",79);
        map.put("LASTORE",80);
        map.put("FASTORE",81);
        map.put("DASTORE",82);
        map.put("AASTORE",83);
        map.put("BASTORE",84);
        map.put("CASTORE",85);
        map.put("SASTORE",86);
        map.put("POP",87);
        map.put("POP2",88);
        map.put("DUP",89);
        map.put("DUP_X1",90);
        map.put("DUP_X2",91);
        map.put("DUP2",92);
        map.put("DUP2_X1",93);
        map.put("DUP2_X2",94);
        map.put("SWAP",95);
        map.put("IADD",96);
        map.put("LADD",97);
        map.put("FADD",98);
        map.put("DADD",99);
        map.put("ISUB",100);
        map.put("LSUB",101);
        map.put("FSUB",102);
        map.put("DSUB",103);
        map.put("IMUL",104);
        map.put("LMUL",105);
        map.put("FMUL",106);
        map.put("DMUL",107);
        map.put("IDIV",108);
        map.put("LDIV",109);
        map.put("FDIV",110);
        map.put("DDIV",111);
        map.put("IREM",112);
        map.put("LREM",113);
        map.put("FREM",114);
        map.put("DREM",115);
        map.put("INEG",116);
        map.put("LNEG",117);
        map.put("FNEG",118);
        map.put("DNEG",119);
        map.put("ISHL",120);
        map.put("LSHL",121);
        map.put("ISHR",122);
        map.put("LSHR",123);
        map.put("IUSHR",124);
        map.put("LUSHR",125);
        map.put("IAND",126);
        map.put("LAND",127);
        map.put("IOR",128);
        map.put("LOR",129);
        map.put("IXOR",130);
        map.put("LXOR",131);
        map.put("IINC",132);
        map.put("I2L",133);
        map.put("I2F",134);
        map.put("I2D",135);
        map.put("L2I",136);
        map.put("L2F",137);
        map.put("L2D",138);
        map.put("F2I",139);
        map.put("F2L",140);
        map.put("F2D",141);
        map.put("D2I",142);
        map.put("D2L",143);
        map.put("D2F",144);
        map.put("I2B",145);
        map.put("I2C",146);
        map.put("I2S",147);
        map.put("LCMP",148);
        map.put("FCMPL",149);
        map.put("FCMPG",150);
        map.put("DCMPL",151);
        map.put("DCMPG",152);
        map.put("IFEQ",153);
        map.put("IFNE",154);
        map.put("IFLT",155);
        map.put("IFGE",156);
        map.put("IFGT",157);
        map.put("IFLE",158);
        map.put("IF_ICMPEQ",159);
        map.put("IF_ICMPNE",160);
        map.put("IF_ICMPLT",161);
        map.put("IF_ICMPGE",162);
        map.put("IF_ICMPGT",163);
        map.put("IF_ICMPLE",164);
        map.put("IF_ACMPEQ",165);
        map.put("IF_ACMPNE",166);
        map.put("GOTO",167);
        map.put("JSR",168);
        map.put("RET",169);
        map.put("TABLESWITCH",170);
        map.put("LOOKUPSWITCH",171);
        map.put("IRETURN",172);
        map.put("LRETURN",173);
        map.put("FRETURN",174);
        map.put("DRETURN",175);
        map.put("ARETURN",176);
        map.put("RETURN",177);
        map.put("GETSTATIC",178);
        map.put("PUTSTATIC",179);
        map.put("GETFIELD",180);
        map.put("PUTFIELD",181);
        map.put("INVOKEVIRTUAL",182);
        map.put("INVOKESPECIAL",183);
        map.put("INVOKESTATIC",184);
        map.put("INVOKEINTERFACE",185);
        map.put("INVOKEDYNAMIC",186);
        map.put("NEW",187);
        map.put("NEWARRAY",188);
        map.put("ANEWARRAY",189);
        map.put("ARRAYLENGTH",190);
        map.put("ATHROW",191);
        map.put("CHECKCAST",192);
        map.put("INSTANCEOF",193);
        map.put("MONITORENTER",194);
        map.put("MONITOREXIT",195);
        map.put("MULTIANEWARRAY",197);
        map.put("IFNULL",198);
        map.put("IFNONNULL",199); 
        return map;
    }).get();

    public static final Map<Integer,String> opcodeToName = ((Supplier<Map<Integer,String>>)()-> {
        Map<Integer,String> map = Maps.newHashMap();
        for(Entry<String, Integer> entry : nameToOpcodeInt.entrySet()) {
            map.put(entry.getValue(), entry.getKey());
        }
        return map;
    }).get();

    // private static final Map<String,Function<String,AbstractInsnNode>> nameToInsnNode = ((Supplier<Map<String,Function<String,AbstractInsnNode>>>)()-> {
    //     Map<String,Function<String,AbstractInsnNode>> map = Maps.newHashMap();
    //     map.put("NOP", (string) -> new InsnNode(nameToOpcodeInt.get("NOP")));
    //     map.put("ACONST_NULL", (string) -> new InsnNode(nameToOpcodeInt.get("ACONST_NULL")));
    //     map.put("ICONST_M1", (string) -> new InsnNode(nameToOpcodeInt.get("ICONST_M1")));
    //     map.put("ICONST_0", (string) -> new InsnNode(nameToOpcodeInt.get("ICONST_0")));
    //     map.put("ICONST_1", (string) -> new InsnNode(nameToOpcodeInt.get("ICONST_1")));
    //     map.put("ICONST_2", (string) -> new InsnNode(nameToOpcodeInt.get("ICONST_2")));
    //     map.put("ICONST_3", (string) -> new InsnNode(nameToOpcodeInt.get("ICONST_3")));
    //     map.put("ICONST_4", (string) -> new InsnNode(nameToOpcodeInt.get("ICONST_4")));
    //     map.put("ICONST_5", (string) -> new InsnNode(nameToOpcodeInt.get("ICONST_5")));
    //     map.put("LCONST_0", (string) -> new InsnNode(nameToOpcodeInt.get("LCONST_0")));
    //     map.put("LCONST_1", (string) -> new InsnNode(nameToOpcodeInt.get("LCONST_1")));
    //     map.put("FCONST_0", (string) -> new InsnNode(nameToOpcodeInt.get("FCONST_0")));
    //     map.put("FCONST_1", (string) -> new InsnNode(nameToOpcodeInt.get("FCONST_1")));
    //     map.put("FCONST_2", (string) -> new InsnNode(nameToOpcodeInt.get("FCONST_2")));
    //     map.put("DCONST_0", (string) -> new InsnNode(nameToOpcodeInt.get("DCONST_0")));
    //     map.put("DCONST_1", (string) -> new InsnNode(nameToOpcodeInt.get("DCONST_1")));
    //     map.put("BIPUSH", (string) -> new IntInsnNode(nameToOpcodeInt.get("BIPUSH"), Integer.parseInt(string.replace("BIPUSH ", "")))); 
    //     map.put("SIPUSH", (string) -> new IntInsnNode(nameToOpcodeInt.get("SIPUSH"), Integer.parseInt(string.replace("SIPUSH ", "")))); 
    //     map.put("LDC", (string) -> {
    //         String number = string.replace("LDC ","");
    //         try {
    //             return new LdcInsnNode(Integer.parseInt(number));
    //         } catch(Throwable t){}
    //         try {
    //             return new LdcInsnNode(Long.parseLong(number));
    //         } catch(Throwable t){}
    //         try {
    //             return new LdcInsnNode(Double.parseDouble(number));
    //         } catch(Throwable t){}
    //         try {
    //             return new LdcInsnNode(Float.parseFloat(number));
    //         } catch(Throwable t){}
    //         return new LdcInsnNode(number);
    //     });
    //     map.put("ILOAD", (string) -> new VarInsnNode(nameToOpcodeInt.get("ILOAD"), Integer.parseInt(string.replace("ILOAD ", ""))));
    //     map.put("LLOAD", (string) -> new VarInsnNode(nameToOpcodeInt.get("LLOAD"), Integer.parseInt(string.replace("LLOAD ", ""))));
    //     map.put("FLOAD", (string) -> new VarInsnNode(nameToOpcodeInt.get("FLOAD"), Integer.parseInt(string.replace("FLOAD ", ""))));
    //     map.put("DLOAD", (string) -> new VarInsnNode(nameToOpcodeInt.get("DLOAD"), Integer.parseInt(string.replace("DLOAD ", ""))));
    //     map.put("ALOAD", (string) -> new VarInsnNode(nameToOpcodeInt.get("ALOAD"), Integer.parseInt(string.replace("ALOAD ", ""))));
    //     map.put("IALOAD", (string) -> new InsnNode(nameToOpcodeInt.get("IALOAD")));
    //     map.put("LALOAD", (string) -> new InsnNode(nameToOpcodeInt.get("LALOAD")));
    //     map.put("FALOAD", (string) -> new InsnNode(nameToOpcodeInt.get("FALOAD")));
    //     map.put("DALOAD", (string) -> new InsnNode(nameToOpcodeInt.get("DALOAD")));
    //     map.put("AALOAD", (string) -> new InsnNode(nameToOpcodeInt.get("AALOAD")));
    //     map.put("BALOAD", (string) -> new InsnNode(nameToOpcodeInt.get("BALOAD")));
    //     map.put("CALOAD", (string) -> new InsnNode(nameToOpcodeInt.get("CALOAD")));
    //     map.put("SALOAD", (string) -> new InsnNode(nameToOpcodeInt.get("SALOAD")));
    //     map.put("ISTORE", (string) -> new VarInsnNode(nameToOpcodeInt.get("ISTORE"), Integer.parseInt(string.replace("ISTORE ", ""))));
    //     map.put("LSTORE", (string) -> new VarInsnNode(nameToOpcodeInt.get("LSTORE"), Integer.parseInt(string.replace("LSTORE ", ""))));
    //     map.put("FSTORE", (string) -> new VarInsnNode(nameToOpcodeInt.get("FSTORE"), Integer.parseInt(string.replace("FSTORE ", ""))));
    //     map.put("DSTORE", (string) -> new VarInsnNode(nameToOpcodeInt.get("DSTORE"), Integer.parseInt(string.replace("DSTORE ", ""))));
    //     map.put("ASTORE", (string) -> new VarInsnNode(nameToOpcodeInt.get("ASTORE"), Integer.parseInt(string.replace("ASTORE ", ""))));
    //     map.put("IASTORE", (string) -> new InsnNode(nameToOpcodeInt.get("IASTORE")));
    //     map.put("LASTORE", (string) -> new InsnNode(nameToOpcodeInt.get("LASTORE")));
    //     map.put("FASTORE", (string) -> new InsnNode(nameToOpcodeInt.get("FASTORE")));
    //     map.put("DASTORE", (string) -> new InsnNode(nameToOpcodeInt.get("DASTORE")));
    //     map.put("AASTORE", (string) -> new InsnNode(nameToOpcodeInt.get("AASTORE")));
    //     map.put("BASTORE", (string) -> new InsnNode(nameToOpcodeInt.get("BASTORE")));
    //     map.put("CASTORE", (string) -> new InsnNode(nameToOpcodeInt.get("CASTORE")));
    //     map.put("SASTORE", (string) -> new InsnNode(nameToOpcodeInt.get("SASTORE")));
    //     map.put("POP", (string) -> new InsnNode(nameToOpcodeInt.get("POP")));
    //     map.put("POP2", (string) -> new InsnNode(nameToOpcodeInt.get("POP2")));
    //     map.put("DUP", (string) -> new InsnNode(nameToOpcodeInt.get("DUP")));
    //     map.put("DUP_X1", (string) -> new InsnNode(nameToOpcodeInt.get("DUP_X1")));
    //     map.put("DUP_X2", (string) -> new InsnNode(nameToOpcodeInt.get("DUP_X2")));
    //     map.put("DUP2", (string) -> new InsnNode(nameToOpcodeInt.get("DUP2")));
    //     map.put("DUP2_X1", (string) -> new InsnNode(nameToOpcodeInt.get("DUP2_X1")));
    //     map.put("DUP2_X2", (string) -> new InsnNode(nameToOpcodeInt.get("DUP2_X2")));
    //     map.put("SWAP", (string) -> new InsnNode(nameToOpcodeInt.get("SWAP")));
    //     map.put("IADD", (string) -> new InsnNode(nameToOpcodeInt.get("IADD")));
    //     map.put("LADD", (string) -> new InsnNode(nameToOpcodeInt.get("LADD")));
    //     map.put("FADD", (string) -> new InsnNode(nameToOpcodeInt.get("FADD")));
    //     map.put("DADD", (string) -> new InsnNode(nameToOpcodeInt.get("DADD")));
    //     map.put("ISUB", (string) -> new InsnNode(nameToOpcodeInt.get("ISUB")));
    //     map.put("LSUB", (string) -> new InsnNode(nameToOpcodeInt.get("LSUB")));
    //     map.put("FSUB", (string) -> new InsnNode(nameToOpcodeInt.get("FSUB")));
    //     map.put("DSUB", (string) -> new InsnNode(nameToOpcodeInt.get("DSUB")));
    //     map.put("IMUL", (string) -> new InsnNode(nameToOpcodeInt.get("IMUL")));
    //     map.put("LMUL", (string) -> new InsnNode(nameToOpcodeInt.get("LMUL")));
    //     map.put("FMUL", (string) -> new InsnNode(nameToOpcodeInt.get("FMUL")));
    //     map.put("DMUL", (string) -> new InsnNode(nameToOpcodeInt.get("DMUL")));
    //     map.put("IDIV", (string) -> new InsnNode(nameToOpcodeInt.get("IDIV")));
    //     map.put("LDIV", (string) -> new InsnNode(nameToOpcodeInt.get("LDIV")));
    //     map.put("FDIV", (string) -> new InsnNode(nameToOpcodeInt.get("FDIV")));
    //     map.put("DDIV", (string) -> new InsnNode(nameToOpcodeInt.get("DDIV")));
    //     map.put("IREM", (string) -> new InsnNode(nameToOpcodeInt.get("IREM")));
    //     map.put("LREM", (string) -> new InsnNode(nameToOpcodeInt.get("LREM")));
    //     map.put("FREM", (string) -> new InsnNode(nameToOpcodeInt.get("FREM")));
    //     map.put("DREM", (string) -> new InsnNode(nameToOpcodeInt.get("DREM")));
    //     map.put("INEG", (string) -> new InsnNode(nameToOpcodeInt.get("INEG")));
    //     map.put("LNEG", (string) -> new InsnNode(nameToOpcodeInt.get("LNEG")));
    //     map.put("FNEG", (string) -> new InsnNode(nameToOpcodeInt.get("FNEG")));
    //     map.put("DNEG", (string) -> new InsnNode(nameToOpcodeInt.get("DNEG")));
    //     map.put("ISHL", (string) -> new InsnNode(nameToOpcodeInt.get("ISHL")));
    //     map.put("LSHL", (string) -> new InsnNode(nameToOpcodeInt.get("LSHL")));
    //     map.put("ISHR", (string) -> new InsnNode(nameToOpcodeInt.get("ISHR")));
    //     map.put("LSHR", (string) -> new InsnNode(nameToOpcodeInt.get("LSHR")));
    //     map.put("IUSHR", (string) -> new InsnNode(nameToOpcodeInt.get("IUSHR")));
    //     map.put("LUSHR", (string) -> new InsnNode(nameToOpcodeInt.get("LUSHR")));
    //     map.put("IAND", (string) -> new InsnNode(nameToOpcodeInt.get("IAND")));
    //     map.put("LAND", (string) -> new InsnNode(nameToOpcodeInt.get("LAND")));
    //     map.put("IOR", (string) -> new InsnNode(nameToOpcodeInt.get("IOR")));
    //     map.put("LOR", (string) -> new InsnNode(nameToOpcodeInt.get("LOR")));
    //     map.put("IXOR", (string) -> new InsnNode(nameToOpcodeInt.get("IXOR")));
    //     map.put("LXOR", (string) -> new InsnNode(nameToOpcodeInt.get("LXOR")));
    //     map.put("IINC", (string) -> new InsnNode(nameToOpcodeInt.get("IINC")));
    //     map.put("I2L", (string) -> new InsnNode(nameToOpcodeInt.get("I2L")));
    //     map.put("I2F", (string) -> new InsnNode(nameToOpcodeInt.get("I2F")));
    //     map.put("I2D", (string) -> new InsnNode(nameToOpcodeInt.get("I2D")));
    //     map.put("L2I", (string) -> new InsnNode(nameToOpcodeInt.get("L2I")));
    //     map.put("L2F", (string) -> new InsnNode(nameToOpcodeInt.get("L2F")));
    //     map.put("L2D", (string) -> new InsnNode(nameToOpcodeInt.get("L2D")));
    //     map.put("F2I", (string) -> new InsnNode(nameToOpcodeInt.get("F2I")));
    //     map.put("F2L", (string) -> new InsnNode(nameToOpcodeInt.get("F2L")));
    //     map.put("F2D", (string) -> new InsnNode(nameToOpcodeInt.get("F2D")));
    //     map.put("D2I", (string) -> new InsnNode(nameToOpcodeInt.get("D2I")));
    //     map.put("D2L", (string) -> new InsnNode(nameToOpcodeInt.get("D2L")));
    //     map.put("D2F", (string) -> new InsnNode(nameToOpcodeInt.get("D2F")));
    //     map.put("I2B", (string) -> new InsnNode(nameToOpcodeInt.get("I2B")));
    //     map.put("I2C", (string) -> new InsnNode(nameToOpcodeInt.get("I2C")));
    //     map.put("I2S", (string) -> new InsnNode(nameToOpcodeInt.get("I2S")));
    //     map.put("LCMP", (string) -> new InsnNode(nameToOpcodeInt.get("LCMP")));
    //     map.put("FCMPL", (string) -> new InsnNode(nameToOpcodeInt.get("FCMPL")));
    //     map.put("FCMPG", (string) -> new InsnNode(nameToOpcodeInt.get("FCMPG")));
    //     map.put("DCMPL", (string) -> new InsnNode(nameToOpcodeInt.get("DCMPL")));
    //     map.put("DCMPG", (string) -> new InsnNode(nameToOpcodeInt.get("DCMPG")));
    //     map.put("IFEQ", (string)-> null); // "([A-Z_0-9]{1,})", \(string\)-> null     "$1", (string) -> new InsnNode(lookup.get("$1"))
    //     map.put("IFNE", (string)-> null);
    //     map.put("IFLT", (string)-> null);
    //     map.put("IFGE", (string)-> null);
    //     map.put("IFGT", (string)-> null);
    //     map.put("IFLE", (string)-> null);
    //     map.put("IF_ICMPEQ", (string)-> null);
    //     map.put("IF_ICMPNE", (string)-> null);
    //     map.put("IF_ICMPLT", (string)-> null);
    //     map.put("IF_ICMPGE", (string)-> null);
    //     map.put("IF_ICMPGT", (string)-> null);
    //     map.put("IF_ICMPLE", (string)-> null);
    //     map.put("IF_ACMPEQ", (string)-> null);
    //     map.put("IF_ACMPNE", (string)-> null);
    //     map.put("GOTO", (string)-> null);
    //     map.put("JSR", (string)-> null);
    //     map.put("RET", (string)-> null);
    //     map.put("TABLESWITCH", (string)-> null); 
    //     map.put("LOOKUPSWITCH", (string)-> null);
    //     map.put("IRETURN", (string) -> new InsnNode(nameToOpcodeInt.get("IRETURN")));
    //     map.put("LRETURN", (string) -> new InsnNode(nameToOpcodeInt.get("LRETURN")));
    //     map.put("FRETURN", (string) -> new InsnNode(nameToOpcodeInt.get("FRETURN")));
    //     map.put("DRETURN", (string) -> new InsnNode(nameToOpcodeInt.get("DRETURN")));
    //     map.put("ARETURN", (string) -> new InsnNode(nameToOpcodeInt.get("ARETURN")));
    //     map.put("RETURN", (string) -> new InsnNode(nameToOpcodeInt.get("RETURN")));
    //     map.put("GETSTATIC", (string) -> new FieldInsnNode(nameToOpcodeInt.get("GETSTATIC"), string.split(" ")[1], string.split(" ")[2], string.split(" ")[3]));
    //     map.put("PUTSTATIC", (string) -> new FieldInsnNode(nameToOpcodeInt.get("PUTSTATIC"), string.split(" ")[1], string.split(" ")[2], string.split(" ")[3]));
    //     map.put("GETFIELD", (string) -> new FieldInsnNode(nameToOpcodeInt.get("GETFIELD"), string.split(" ")[1], string.split(" ")[2], string.split(" ")[3]));
    //     map.put("PUTFIELD", (string) -> new FieldInsnNode(nameToOpcodeInt.get("PUTFIELD"), string.split(" ")[1], string.split(" ")[2], string.split(" ")[3]));
    //     map.put("INVOKEVIRTUAL", (string) -> new MethodInsnNode(nameToOpcodeInt.get("INVOKEVIRTUAL"), string.split(" ")[1], string.split(" ")[2], string.split(" ")[3]));
    //     map.put("INVOKESPECIAL", (string) -> new MethodInsnNode(nameToOpcodeInt.get("INVOKESPECIAL"), string.split(" ")[1], string.split(" ")[2], string.split(" ")[3]));
    //     map.put("INVOKESTATIC", (string) -> new MethodInsnNode(nameToOpcodeInt.get("INVOKESTATIC"), string.split(" ")[1], string.split(" ")[2], string.split(" ")[3]));
    //     map.put("INVOKEINTERFACE", (string) -> new MethodInsnNode(nameToOpcodeInt.get("INVOKEINTERFACE"), string.split(" ")[1], string.split(" ")[2], string.split(" ")[3]));
    //     map.put("INVOKEDYNAMIC", (string)-> null); 
    //     map.put("NEW", (string) -> new TypeInsnNode(nameToOpcodeInt.get("NEW"), string.split(" ")[1]));
    //     map.put("NEWARRAY", (string)-> new IntInsnNode(nameToOpcodeInt.get("NEWARRAY"), Integer.parseInt(string.split("NEWARRAY ")[1])));
    //     map.put("ANEWARRAY", (string) -> new TypeInsnNode(nameToOpcodeInt.get("ANEWARRAY"), string.split(" ")[1]));
    //     map.put("ARRAYLENGTH", (string)-> new InsnNode(nameToOpcodeInt.get("ARRAYLENGTH")));
    //     map.put("ATHROW", (string)-> null);
    //     map.put("CHECKCAST", (string) -> new TypeInsnNode(nameToOpcodeInt.get("CHECKCAST"), string.split(" ")[1]));
    //     map.put("INSTANCEOF", (string) -> new TypeInsnNode(nameToOpcodeInt.get("INSTANCEOF"), string.split(" ")[1]));
    //     map.put("MONITORENTER", (string)-> null);
    //     map.put("MONITOREXIT", (string)-> null);
    //     map.put("MULTIANEWARRAY", (string)-> null);
    //     map.put("IFNULL", (string)-> null);
    //     map.put("IFNONNULL", (string)-> null);
    //     // CUSTOM
    //     map.put("FIND_AND_LOAD", (string)-> new ConditionalLoadInsn(string.split(" ")[1]));

    //     return map;
    // }).get();

}
