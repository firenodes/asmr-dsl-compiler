package biom4st3r.asmr_compiler.util;

import java.util.Arrays;

import org.objectweb.asm.Type;

public class MethodDesc {
    private final Type[] args;
    public final Type returntype;
    public Type[] getArgs() {
        return Arrays.copyOf(args, args.length); 
    }
    public MethodDesc(String desc) {
        this(Type.getArgumentTypes(desc), Type.getReturnType(desc));
    }
    public MethodDesc(Type[] args, Type returnType) {
        this.args = args;
        this.returntype = returnType;
    }
    public MethodDesc withReturnType(Type type) {
        return new MethodDesc(this.args, type);
    }
    public MethodDesc withArg(int index, Type type) {
        Type[] args = Arrays.copyOf(this.args, this.args.length);
        args[index] = type;
        return new MethodDesc(args, this.returntype);
    }
    public MethodDesc withArgs(Type[] type) {
        return new MethodDesc(type, this.returntype);
    }
    @Override
    public String toString() {
        return Type.getMethodDescriptor(this.returntype, this.args);
    }
    public String toDesc() {
        return this.toString();
    }
	public MethodDesc replaceMatching(Type targetType, Type replacingType) {
        MethodDesc desc = this;
        if(desc.returntype.equals(targetType)) {
            desc = desc.withReturnType(replacingType);
        }
        for (int i = 0; i < desc.args.length; i++) {
            if(desc.args[i].equals(targetType)) {
                desc = desc.withArg(i, replacingType);
            }
        }
        return desc;
	}
}