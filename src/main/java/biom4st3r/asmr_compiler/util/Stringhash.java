package biom4st3r.asmr_compiler.util;

import java.util.UUID;

public class Stringhash {
    static final long NUM = 9223372036854775783L;

    public static String getHash(String string) {
        long l = NUM;
        long l2 = NUM;
        for(byte c : string.getBytes()) {
            l ^= c;
            l = Long.rotateLeft(l, 5);
            
            l2 ^= c;
            l2 = Long.rotateLeft(l2, 7);
        }
        return new UUID(l, l2).toString().replace("-", "_");
    }


}
