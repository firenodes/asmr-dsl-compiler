package biom4st3r.asmr_compiler.util;

import org.objectweb.asm.ClassWriter;
import org.quiltmc.asmr.processor.AsmrClassWriter;
import org.quiltmc.asmr.processor.AsmrProcessor;
import org.quiltmc.asmr.processor.tree.member.AsmrClassNode;

public class AsmrUtil {

    // public static Class getClass(AsmrClassNode node) {
    //     ClassWriter cw = new org.objectweb.asm.ClassWriter(org.objectweb.asm.ClassWriter.COMPUTE_FRAMES);
    //     node.accept(cw);
    //     ClassLoader loader = new ClassLoader(){};
    //     Thread.currentThread().setContextClassLoader(loader);
    //     Method defineClass;
    //     try {
    //         defineClass = ClassLoader.class.getDeclaredMethod("defineClass", byte[].class, int.class, int.class);
    //         defineClass.setAccessible(true);
    //         Class clazz = (Class) defineClass.invoke(loader, cw.toByteArray(), 0, cw.toByteArray().length);
    //         return clazz;
    //     } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
    //         // TODO Auto-generated catch block
    //         e.printStackTrace();
    //     }

    //     return null;
    // }
    public static Class<?> defineClass(AsmrProcessor processor, AsmrClassNode classNode) {
		String className = classNode.name().value().replace('/', '.');

		ClassWriter writer = new AsmrClassWriter(processor);
		classNode.accept(writer);
		byte[] bytecode = writer.toByteArray();

		ClassLoader classLoader = new ClassLoader(Thread.currentThread().getContextClassLoader()) {
			private Class<?> customClass = null;

			@Override
			protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
				Class<?> loadedClass = findLoadedClass(name);
				if (loadedClass == null) {
					try {
						loadedClass = findClass(name);
					} catch (ClassNotFoundException ignore) {
						return super.loadClass(name, resolve);
					}
				}

				if (resolve) {
					resolveClass(loadedClass);
				}
				return loadedClass;
			}

			@Override
			protected Class<?> findClass(String name) throws ClassNotFoundException {
				if (className.equals(name)) {
					if (customClass == null) {
						customClass = defineClass(className, bytecode, 0, bytecode.length);
					}
					return customClass;
				}
				return super.findClass(name);
			}
		};
		try {
			return classLoader.loadClass(className);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
}
