package biom4st3r.asmr_compiler.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Lists {
    public static <T> List<T> newArrayList() {
        return new ArrayList<>();
    }

    @SuppressWarnings({"unchecked"})
    public static <T> List<T> newArrayList(T... objs) {
        List<T> list = new ArrayList<>(objs.length);
        Collections.addAll(list, objs);
        return list;
    }
}
