package biom4st3r.asmr_compiler;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.quiltmc.asmr.processor.AsmrProcessor;
import org.quiltmc.asmr.processor.AsmrTransformer;

import biom4st3r.asmr_compiler.asm_accessed.Description;
import biom4st3r.asmr_compiler.asm_accessed.InjectTransformerDesc;
import biom4st3r.asmr_compiler.asm_accessed.MatchingInfo;
import biom4st3r.asmr_compiler.asm_accessed.MethodTarget;
import biom4st3r.asmr_compiler.asm_accessed.ObjectReplacementDesc;
import biom4st3r.asmr_compiler.util.Lists;
import biom4st3r.asmr_compiler.util.Maps;

public class Compiler {

    public static enum Transformers {
        v0_INJECT("v0 inject"),
        v0_REPLACE_OBJ("v0 objreplacer"),
        ;
        private String header;

        Transformers(String header) {
            this.header = header;
        }
        public boolean matches(Tree tree) {
            return tree.getJoinedInfo().toLowerCase().equals(header);
        }
        public static Transformers getType(Tree tree) {
            Optional<Transformers> o = Stream.of(values()).filter(t->t.matches(tree)).findFirst();
            if(o.isPresent()) return o.get();
            return null;
        }
    }

    public static Class<? extends AsmrTransformer> parseTransformer(AsmrProcessor processor, File file) {
        String[] insns = null;
        try {
            insns = getInsns(file);
        } catch (IOException e) {}
        List<Tree> trees = growTrees(insns);
        Description desc;
        switch(Transformers.getType(trees.get(0))) {
            case v0_INJECT:
                desc = InjectCompiler.buildInjectTransformer(trees);            
                break;
            case v0_REPLACE_OBJ:
                desc = ObjReplacerCompiler.buildReplacerTransformer(trees);
                break;
            default:
                throw new Biom4st3rCompilerError("Invalid header found! %s", trees.get(0).getJoinedInfo());
        }
        desc.setSourceFileName(file.getName().replace(".asmr", ""));
        Class<? extends AsmrTransformer> clazz = desc.buildTransformerClass(processor);
        return clazz;
    }

    public static class Tree {
        public String[] info;
        public final Tree parent;
        public final List<Tree> children = Lists.newArrayList();
        public Tree(String info, Tree parent) {
            this.info = info.split("[\t ]");
            this.parent = parent;
        }
        public String getJoinedInfo() {
            return String.join(" ", info);
        }
        public void reassignInfo(String s) {
            this.info = s.split("[\t ]");
        } 
        public void addChild(Tree tree) {
            this.children.add(tree);
        }
    }
    
    public static String[] getInsns(File file) throws IOException {
        if(!file.isFile()) new Biom4st3rCompilerError("%s is not a file.", file.getName());
        if(!file.getName().endsWith(".asmr")) new Biom4st3rCompilerError("%s doesn't end in .asmr", file.getName());
        FileInputStream stream = new FileInputStream(file);
        return getInsn(stream, file.getName());
    }

    public static String[] getInsn(InputStream stream, String name) throws IOException {
        try {
            byte[] content = stream.readNBytes(Integer.MAX_VALUE);
            stream.close();
            return new String(content).split("\n");
        } catch(NullPointerException e) {
            throw new Biom4st3rCompilerError("%s contents could not be read or was empty.", name);
        }

    }

    public static List<Tree> growTrees(String[] string) {
        List<Tree> list = new ArrayList<>(string.length);
        int prevIndent = 0;
        Tree prevTree = null;
        for(String op : string) {
            int indent = indentionLevel(op);
            if(prevIndent >= indent) {
                prevIndent = indent;
                if(prevTree == null || prevTree.parent == null || indent == 0) {
                    prevTree = new Tree(removeIndent(op), null);
                    list.add(prevTree);
                } else if(prevTree.parent != null) {
                     prevTree.parent.children.add(new Tree(removeIndent(op), prevTree.parent));
                }
            } else if(prevIndent < indent) {
                prevIndent = indent;
                Tree child = new Tree(removeIndent(op),prevTree);
                prevTree.addChild(child);
                prevTree = child;
            }
        }
        if(list.size() <= 0) throw new Biom4st3rCompilerError("Instruction Found, but tree returned empty");
        return list;
    }

    public static int indentionLevel(String s) {
        int i = 0;
        for(char c : s.toCharArray()) {
            if(c == ' ' || c == '\t') {
                i++;
            } else break;
        }
        return i;
    }
    public static class Pair<A,B> {
        public final A left;
        public final B right;
        public Pair(A left, B right) {
            this.left = left;
            this.right = right;
        }
        public Pair<A,B> withLeft(A newLeft) {
            return new Pair<A,B>(newLeft, this.right);
        }
        public Pair<A,B> withRight(B newRight) {
            return new Pair<A,B>(this.left, newRight);
        }
    }

    public static class InjectCompiler extends Compiler {
        public static InjectTransformerDesc buildInjectTransformer(List<Tree> trees) {
            if(!trees.get(0).info[0].equals("v0") || !trees.get(0).info[1].equals("inject")) throw new Biom4st3rCompilerError("Transformer description is not valid for inject: %s", trees.get(0).getJoinedInfo());
            enactMacros(trees);
            InjectTransformerDesc transformer = new InjectTransformerDesc();
            transformer.classTargets.addAll(trees.stream().filter((tree)->Insns.CLASS.matches(tree)).map((tree)->tree.info[1]).collect(Collectors.toList()));
            removeTree(trees, tree->Insns.CLASS.matches(tree));
            try {
                transformer.methodTargets.addAll(trees.stream().filter((tree)->Insns.METHOD.matches(tree)).map((tree)->new MethodTarget(tree.info)).collect(Collectors.toList()));
            } catch(java.lang.ArrayIndexOutOfBoundsException error) {
                throw new Biom4st3rCompilerError("Malformed Method instruction. Proper format 'method theMethodName (theDesc)V'");
            }
            removeTree(trees, tree->Insns.METHOD.matches(tree));
            transformer.matchingInfo.addAll(buildMatchingInfo(trees));
            removeTree(trees, tree->Insns.MATCH.matches(tree));
            getBody(trees.stream().filter((tree)->Insns.BODY.matches(tree)).findFirst().get()).forEach((s)-> {
                transformer.addInsn(s);
            });
            return transformer;
        }
    
        private static List<String> getBody(Tree tree) {
            List<String> list = Lists.newArrayList();
            if(!tree.info[1].equals("bytecode")) throw new Biom4st3rCompilerError("%s is not a valid body type", tree.info[1]);
            for(Tree child : tree.children) {
                list.add(child.getJoinedInfo());
            }
            return list;
        }
    
        private static List<MatchingInfo> buildMatchingInfo(List<Tree> trees) {
            List<MatchingInfo> list = Lists.newArrayList();
            MatchingInfo info = new MatchingInfo();
            for(Tree tree : trees) {
                if(!Insns.MATCH.matches(tree)) continue;
                if("start".equals(tree.info[1])) {    
                    for(Tree child : tree.children) {
                        ConverterDoDad.nameToOpcodeInt.computeIfAbsent(child.info[0], string->{throw new Biom4st3rCompilerError("%s is not a valid java opcodes", child.getJoinedInfo());});
                        info.opcodeList.add(child.info);
                    }
                } else {
                    for(Tree child : tree.children) {
                        if(Insns.ORDINAL.matches(child)) {
                            String number = child.info[1];
                            info.ordinal = Integer.parseInt(number);
                        } else if(Insns.POSITION.matches(child)) {
                            String pos = child.info[1];
                            info.position = pos.toLowerCase().equals("before") ? Position.BEFORE : pos.toLowerCase().equals("after") ? Position.AFTER : null;
                            if(info.position == null) {
                                throw new Biom4st3rCompilerError("Invalid matching position child: %s", child.getJoinedInfo());
                            }
                        }
                    }
                    list.add(info);
                    info = new MatchingInfo();
                }
            }
            return list;
        }
    }

    public static class ObjReplacerCompiler extends Compiler {
        public static ObjectReplacementDesc buildReplacerTransformer(List<Tree> trees) {
            if(!trees.get(0).info[0].equals("v0") || !trees.get(0).info[1].equals("objreplacer")) throw new Biom4st3rCompilerError("Transformer description is not valid for objreplace: %s", trees.get(0).getJoinedInfo());
            enactMacros(trees);
            ObjectReplacementDesc desc = new ObjectReplacementDesc();
            trees.stream().filter(tree->Insns.CLASS.matches(tree)).forEach(tree-> {
                if(Insns.CLASS_TARGET.matches(tree)) {
                    desc.targetClass = tree.info[2];
                } else if(Insns.CLASS_REPLACER.matches(tree)) {
                    desc.replacingClass = tree.info[2];
                }
            });
            trees.stream().filter(tree->Insns.SWAP.matches(tree)).forEach(tree-> {
                desc.targets.add(new Pair<MethodTarget,MethodTarget>(new  MethodTarget(tree.children.get(0).info), new MethodTarget(tree.children.get(1).info)));     
            });

            return desc;
        }
    }

    private static String removeIndent(String s) {
        int i = 0;
        for(char c : s.toCharArray()) {
            if(c == ' ' || c == '\t') {
                i++;
            } else break;
        }
        return s.substring(i).replace("\r", "").replace("\n", "");
    }

    private static void removeTree(List<Tree> trees, Predicate<Tree> tree) {
        for(int i = 0; i < trees.size(); /**no op */) {
            if(tree.test(trees.get(i))) {
                trees.remove(i);
            } else {
                i++;
            }
        }
    }

    private static void enactMacros(List<Tree> trees) {
        Map<String,String> replacements = Maps.newHashMap();
        // Find Macros
        trees.forEach((tree)-> {
            if(Insns.DEFINE.matches(tree)) {
                replacements.put(tree.info[1], tree.info[2]);
            }
        });
        // Enact Macros
        List<Tree> allTrees = explodeTree(trees);
        for(int i = 0; i < allTrees.size(); i++) {
            for(Entry<String, String> entry : replacements.entrySet()) {
                String info = allTrees.get(i).getJoinedInfo();
                allTrees.get(i).reassignInfo(info.replace(entry.getKey(), entry.getValue()));
            }
        }
        removeTree(trees, tree->Insns.DEFINE.matches(tree));
    }

    private static List<Tree> explodeTree(List<Tree> trees) {
        List<Tree> allTrees = Lists.newArrayList();
        for(int i = 0; i < trees.size(); i++) {
            Tree tree = trees.get(i);
            allTrees.add(tree);
            allTrees.addAll(explodeTree(tree.children));
        }
        return allTrees;
    }

    public enum Position {
        BEFORE,
        AFTER,
        ;
    }

    public enum Insns {
        DEFINE("define"),
        CLASS("class"),
        METHOD("method"),
        MATCH("match"),
        ORDINAL("ordinal"), 
        BODY("body"),
        POSITION("position"),
        // obj replacer
        CLASS_TARGET("target",1),
        CLASS_REPLACER("replacer",1), 
        SWAP("swap"),
        ;
        private String name;
        private int index;

        Insns(String name) {
            this.name = name;
            this.index = 0;
        }
        Insns(String name, int depth) {
            this(name);
            this.index = depth;
        }
        public boolean matches(Tree s) {
            return s.info[index].equals(this.name);
        }
    }

}
