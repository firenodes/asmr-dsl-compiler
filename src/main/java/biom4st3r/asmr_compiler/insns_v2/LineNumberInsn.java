package biom4st3r.asmr_compiler.insns_v2;

import org.quiltmc.asmr.processor.tree.AsmrNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrLineNumberNode;
import org.quiltmc.asmr.processor.tree.method.AsmrMethodBodyNode;

public class LineNumberInsn implements Insn {

    @Override
    public boolean matches(AsmrAbstractInsnNode<?> node) {
        return false;
    }

    @Override
    public AsmrAbstractInsnNode<?> as(AsmrNode<?> node) {
        return new AsmrLineNumberNode();
    }

    @Override
    public void accept(AsmrMethodBodyNode body) {
        Insn.super.accept(body);
    }
    
}
