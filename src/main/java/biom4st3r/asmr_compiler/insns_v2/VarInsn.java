package biom4st3r.asmr_compiler.insns_v2;

import biom4st3r.asmr_compiler.Biom4st3rCompilerError;
import biom4st3r.asmr_compiler.ConverterDoDad;
import org.quiltmc.asmr.processor.tree.AsmrNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrVarInsnNode;
import org.quiltmc.asmr.processor.tree.member.AsmrMethodNode;
import org.quiltmc.asmr.processor.tree.method.AsmrIndex;
import org.quiltmc.asmr.processor.tree.method.AsmrMethodBodyNode;

public class VarInsn implements Insn {
    private final int opcode;
    private final int var;

    public VarInsn(int opcode, int var) {
        this.opcode = opcode;
        this.var = var;
    }

    public VarInsn(int opcode, AsmrIndex index) {
        this.opcode = opcode;
        this.index = index;
        this.var = -1;
    }

    @Override
    public boolean matches(AsmrAbstractInsnNode<?> node) {
        if(node instanceof AsmrVarInsnNode) { 
            AsmrVarInsnNode varNode = (AsmrVarInsnNode) node;
            AsmrMethodNode method = (AsmrMethodNode) node.parent().parent().parent();
            if(method.body().localIndexes().size() < var+1) return false;
            boolean matchesVar = varNode.varIndex().value().equals(method.body().localIndexes().get(var).value());
            return ((AsmrVarInsnNode)node).opcode().value() == this.opcode && matchesVar;
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder
            .append(ConverterDoDad.opcodeToName.get(this.opcode)).append(' ')
            .append(this.var)
            ;
        return builder.toString();
    }

    AsmrIndex index = null;

    @Override
    public AsmrAbstractInsnNode<?> as(AsmrNode<?> node) {
        if(index == null) throw new Biom4st3rCompilerError("VarInsn#as() called before accept().");
        AsmrVarInsnNode insn = new AsmrVarInsnNode(node);
        insn.varIndex().init(index);
        insn.opcode().init(this.opcode);
        return insn;
    }
    
    @Override
    public void accept(AsmrMethodBodyNode body) {
        if(index == null) {
            index = body.localIndexes().get(this.var).value();
        }
    }
}
