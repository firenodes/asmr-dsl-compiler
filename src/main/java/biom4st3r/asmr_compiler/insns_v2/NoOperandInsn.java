package biom4st3r.asmr_compiler.insns_v2;

import biom4st3r.asmr_compiler.ConverterDoDad;
import org.quiltmc.asmr.processor.tree.AsmrNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrNoOperandInsnNode;

public class NoOperandInsn implements Insn {

    private final int opcode;

    public NoOperandInsn(int opcode) {
        this.opcode = opcode;
    }

    @Override
    public boolean matches(AsmrAbstractInsnNode<?> node) {
        if(node instanceof AsmrNoOperandInsnNode) {
            return ((AsmrNoOperandInsnNode)node).opcode().value().equals(this.opcode);
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder
            .append(ConverterDoDad.opcodeToName.get(this.opcode))
            ;
        return builder.toString();
    }

    @Override
    public AsmrAbstractInsnNode<?> as(AsmrNode<?> node) {
        AsmrNoOperandInsnNode insn = new AsmrNoOperandInsnNode(node);
        insn.opcode().init(this.opcode);
        return insn;
    }
    
}

