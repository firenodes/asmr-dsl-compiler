package biom4st3r.asmr_compiler.insns_v2;

import biom4st3r.asmr_compiler.ConverterDoDad;
import org.objectweb.asm.Opcodes;
import org.quiltmc.asmr.processor.tree.AsmrNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrMethodInsnNode;

public class MethodInsn implements Insn {
    private final int opcode;
    private final String owner;
    private final String name;
    private final String desc;

    public MethodInsn(int opcode, String owner, String name, String desc) {
        this.opcode = opcode;
        this.owner = owner;
        this.name = name;
        this.desc = desc;
    }
    static final String skipcode = "#SKIP";
    @Override
    public boolean matches(AsmrAbstractInsnNode<?> node) {
        if(node instanceof AsmrMethodInsnNode) {
            boolean opcodematch = ((AsmrMethodInsnNode)node).opcode().value().equals(this.opcode);
            boolean namematch = ((AsmrMethodInsnNode)node).name().value().equals(this.name) || this.name.equals(skipcode);
            boolean descmatch = ((AsmrMethodInsnNode)node).desc().value().equals(this.desc) || this.desc.equals(skipcode);
            boolean ownermatch = ((AsmrMethodInsnNode)node).owner().value().equals(this.owner) || this.owner.equals(skipcode);
            return opcodematch && namematch && descmatch && ownermatch;
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder
            .append(ConverterDoDad.opcodeToName.get(this.opcode)).append(' ')
            .append(owner).append(' ').append(this.name).append(' ').append(this.desc);
            ;
        return builder.toString();
    }

    @Override
    public AsmrAbstractInsnNode<?> as(AsmrNode<?> node) {
        AsmrMethodInsnNode insn = new AsmrMethodInsnNode(node);
        insn.name().init(this.name);
        insn.owner().init(this.owner);
        insn.desc().init(this.desc);
        insn.opcode().init(this.opcode);
        insn.itf().init(this.opcode == Opcodes.INVOKEINTERFACE);
        return insn;
    }
    
}
