package biom4st3r.asmr_compiler.insns_v2;

import biom4st3r.asmr_compiler.Asm;
import biom4st3r.asmr_compiler.Biom4st3rCompilerError;
import biom4st3r.asmr_compiler.util.TextBuilder;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.quiltmc.asmr.processor.tree.AsmrNode;
import org.quiltmc.asmr.processor.tree.AsmrValueListNode;
import org.quiltmc.asmr.processor.tree.AsmrValueNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.processor.tree.method.AsmrLocalVariableNode;
import org.quiltmc.asmr.processor.tree.method.AsmrMethodBodyNode;

/**
 * Local variable load. Finds and loads the first local variable of {@link #type}
 */
public class LVInsn implements Insn {

    public final String instruction;
    public final int opcode;
    public final Type type;
    @SuppressWarnings({"unused"})
    private static final String
    LOAD = "LVLOAD",
    STORE = "LVSTORE",
    LOAD_ILLEGAL = "LVLOAD_ILLEGAL",
    STORE_ILLEGAL = "LVSTORE_ILLEGAL";
    
    public LVInsn(String instruction, Type type) {
        this.instruction = instruction;
        this.type = type;
        // Opcodes
        this.opcode = instruction.equals(LOAD) || instruction.equals(LOAD_ILLEGAL) ? Asm.get(type).getLoadInsn() : Asm.get(type).getStoreInsn();
    }

    @Override
    public boolean matches(AsmrAbstractInsnNode<?> node) {
        throw new Biom4st3rCompilerError("%s is not supported for matching %s", instruction, type.getInternalName());
    }

    @Override
    public AsmrAbstractInsnNode<?> as(AsmrNode<?> node) {
        return insn.as(node);
    }

    VarInsn insn = null;

    @Override
    public String toString() {
        TextBuilder builder = new TextBuilder();
        builder.append(instruction).sep().append(type.getInternalName());
        return builder.toString();
    }

    @Override
    public void accept(AsmrMethodBodyNode body) {
        for (AsmrLocalVariableNode i : body.localVariables()) {
            if(type.getDescriptor().equals(i.desc().value())) {
                if(this.opcode == Opcodes.ASTORE && body.localVariables().get(0) == i && this.instruction == STORE) {
                    AsmrValueListNode<Integer> list = Insn.getMethodNode(body).modifiers();
                    boolean isStatic = false;
                    for(AsmrValueNode<Integer> val : list) {
                        if(val.value().intValue() == Opcodes.ACC_STATIC) {
                            isStatic = true;
                            break;
                        }
                    }  
                    if(!isStatic) {
                        new Biom4st3rCompilerError("LVSTORE %s resovled to ASTORE 0 in a non-static methods. Use LVSTORE_ILLEGAL if this was intended. In the mean time we'll just skip index 0.", this.type.getInternalName()).printStackTrace();
                        continue;
                    }
                }
                insn = new VarInsn(this.opcode, i.index().value());
                return;
            }
        }
        throw new Biom4st3rCompilerError("LV Insn match not found for %s in %s.%s", this.toString(), Insn.getClassNode(body).name().value() ,Insn.getMethodNode(body).name().value());
    }
    
}
