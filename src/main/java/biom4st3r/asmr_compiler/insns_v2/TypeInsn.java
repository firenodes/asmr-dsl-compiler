package biom4st3r.asmr_compiler.insns_v2;

import biom4st3r.asmr_compiler.ConverterDoDad;
import org.quiltmc.asmr.processor.tree.AsmrNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrTypeInsnNode;

public class TypeInsn implements Insn {
    private final int opcode;
    private final String desc;

    public TypeInsn(int opcode, String desc) {
        this.opcode = opcode;
        this.desc = desc;
    }

    @Override
    public boolean matches(AsmrAbstractInsnNode<?> node) {
        if(node instanceof AsmrTypeInsnNode) {
            return ((AsmrTypeInsnNode)node).opcode().value().equals(this.opcode) && ((AsmrTypeInsnNode)node).desc().value().equals(this.desc);
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder
            .append(ConverterDoDad.opcodeToName.get(this.opcode)).append(' ')
            .append(this.desc)
            ;
        return builder.toString();
    }

    @Override
    public AsmrAbstractInsnNode<?> as(AsmrNode<?> node) {
        AsmrTypeInsnNode insn = new AsmrTypeInsnNode(node);
        insn.desc().init(this.desc);
        insn.opcode().init(this.opcode);
        return insn;
    }
}
