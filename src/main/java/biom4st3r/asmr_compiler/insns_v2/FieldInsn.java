package biom4st3r.asmr_compiler.insns_v2;

import biom4st3r.asmr_compiler.ConverterDoDad;
import org.quiltmc.asmr.processor.tree.AsmrNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrFieldInsnNode;

public class FieldInsn implements Insn {

    private final int opcode;
    private final String owner;
    private final String name;
    private final String desc;

    public FieldInsn(int opcode, String owner, String name, String desc) {
        this.opcode = opcode;
        this.owner = owner;
        this.name = name;
        this.desc = desc;
    }

    @Override
    public boolean matches(AsmrAbstractInsnNode<?> node) {
        if(node instanceof AsmrFieldInsnNode) {
            return ((AsmrFieldInsnNode)node).name().value().equals(this.name) && ((AsmrFieldInsnNode)node).desc().value().equals(this.desc) && ((AsmrFieldInsnNode)node).owner().value().equals(this.owner);
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder
            .append(ConverterDoDad.opcodeToName.get(this.opcode)).append(' ')
            .append(owner).append(' ').append(this.name).append(' ').append(this.desc);
            ;
        return builder.toString();
    }

    @Override
    public AsmrAbstractInsnNode<?> as(AsmrNode<?> node) {
        AsmrFieldInsnNode insn = new AsmrFieldInsnNode(node);
        insn.name().init(this.name);
        insn.owner().init(this.owner);
        insn.desc().init(this.desc);
        insn.opcode().init(this.opcode);
        return insn;
    }
    
}
