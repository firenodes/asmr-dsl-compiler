package biom4st3r.asmr_compiler.insns_v2;

import org.objectweb.asm.Opcodes;
import org.quiltmc.asmr.processor.tree.AsmrNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrLdcInsnNode;

public class LdcInsn implements Insn {
    private final Object cst;

    public LdcInsn(Object cst) {
        this.cst = cst;
    }

    @Override
    public boolean matches(AsmrAbstractInsnNode<?> node) {
        if(node instanceof AsmrLdcInsnNode) {
            return ((AsmrLdcInsnNode)node).cstList().get(0).equals(this.cst); //TODO
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder
            .append("LDC").append(' ')
            .append(this.cst)
            ;
        return builder.toString();
    }

    @Override
    public AsmrAbstractInsnNode<?> as(AsmrNode<?> node) {
        AsmrLdcInsnNode insn = new AsmrLdcInsnNode(node);
        // insn.cstList().addCopy(new AsmrValueNode<Object>(insn)); // TODO
        insn.opcode().init(Opcodes.LDC);
        return insn;
    }
}
