package biom4st3r.asmr_compiler.insns_v2;

import org.quiltmc.asmr.processor.tree.AsmrNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.processor.tree.member.AsmrClassNode;
import org.quiltmc.asmr.processor.tree.member.AsmrMethodNode;
import org.quiltmc.asmr.processor.tree.method.AsmrMethodBodyNode;

public interface Insn {
    boolean matches(AsmrAbstractInsnNode<?> node);
    AsmrAbstractInsnNode<?> as(AsmrNode<?> node);
    default void accept(AsmrMethodBodyNode body) {};
    @Override
    String toString();
    static AsmrMethodNode getMethodNode(AsmrMethodBodyNode node) {
        return (AsmrMethodNode) node.parent();
    }
    static AsmrClassNode getClassNode(AsmrMethodBodyNode node) {
        return (AsmrClassNode) node.parent().parent().parent();
    }
}
