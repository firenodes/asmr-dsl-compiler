package biom4st3r.asmr_compiler.insns_v2;

import biom4st3r.asmr_compiler.ConverterDoDad;
import org.quiltmc.asmr.processor.tree.AsmrNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrIntInsnNode;

public class IntInsn implements Insn {
    private final int opcode;
    private final int operand;

    public IntInsn(int opcode, int operand) {
        this.opcode = opcode;
        this.operand = operand;
    }

    @Override
    public boolean matches(AsmrAbstractInsnNode<?> node) {
        if(node instanceof AsmrIntInsnNode) {
            return ((AsmrIntInsnNode)node).opcode().value().equals(this.opcode) && ((AsmrIntInsnNode)node).operand().value().equals(this.operand);
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder
            .append(ConverterDoDad.opcodeToName.get(this.opcode)).append(' ')
            .append(this.operand)
            ;
        return builder.toString();
    }

    @Override
    public AsmrAbstractInsnNode<?> as(AsmrNode<?> node) {
        AsmrIntInsnNode insn = new AsmrIntInsnNode(node);
        insn.operand().init(this.operand);
        insn.opcode().init(this.opcode);
        return insn;
    }
    
}
