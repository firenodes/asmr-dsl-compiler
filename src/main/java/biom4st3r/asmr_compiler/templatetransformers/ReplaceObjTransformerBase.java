package biom4st3r.asmr_compiler.templatetransformers;

import org.objectweb.asm.tree.TypeInsnNode;
import org.quiltmc.asmr.processor.AsmrProcessor;
import org.quiltmc.asmr.processor.capture.AsmrNodeCapture;
import org.quiltmc.asmr.processor.capture.AsmrSliceCapture;
import org.quiltmc.asmr.processor.tree.AsmrValueNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrFieldInsnNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrMethodInsnNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrTypeInsnNode;
import org.quiltmc.asmr.processor.tree.member.AsmrMethodNode;

import biom4st3r.asmr_compiler.asm_accessed.ObjectReplacementDesc;
import biom4st3r.asmr_compiler.util.MethodDesc;

public abstract class ReplaceObjTransformerBase extends TransformerGenerator {

    @Override
    public void apply(AsmrProcessor arg0) {
        
    }
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void read(AsmrProcessor processsor) {
        processsor.withAllClasses((acn)-> {
            acn.methods().stream().map((amn)-> amn.body().instructions()).forEach((insns)-> {
               for(int i = 0; i < insns.size(); i++) {
                    AsmrAbstractInsnNode insn = insns.get(i);
                    if (this.getDesc().isCanadate(insn)) {
                        if(insn instanceof AsmrMethodInsnNode) {
                            AsmrSliceCapture capture = processsor.refCapture(insns, i, i+1, true, false);
                            processsor.addWrite(this, capture, ()-> {
                                return getDesc().getReplacingInsn((AsmrMethodInsnNode) insn);
                            });
                        } else if(insn instanceof AsmrTypeInsnNode) {
                            AsmrNodeCapture capture = processsor.refCapture(insn);
                            processsor.addWrite(this, capture, ()-> {
                                AsmrTypeInsnNode node = new AsmrTypeInsnNode();
                                node.opcode().init(((AsmrTypeInsnNode)insn).opcode().value());
                                node.desc().init(getDesc().replacingClass);
                                return node;
                            });
                        } else if(insn instanceof AsmrFieldInsnNode) {
                            AsmrNodeCapture capture = processsor.refCapture(((AsmrFieldInsnNode)insn).desc());
                            processsor.addWrite(this, capture, ()-> {
                                AsmrValueNode node = new AsmrValueNode<>();
                                node.init(getDesc().getReplacingType().getDescriptor());
                                return node;
                            });
                        }
                    }
                }
            });
            acn.methods().stream().map(amn->amn.body().localVariables()).forEach(lvt-> {
                lvt.forEach(lv-> {
                    if(lv.desc().value().equals(getDesc().getTargetType().getDescriptor())) {
                        AsmrNodeCapture capture = processsor.refCapture(lv.desc());
                        processsor.addWrite(this, capture, ()-> {
                            AsmrValueNode<String> string = new AsmrValueNode<>();
                            string.init(getDesc().getReplacingType().getDescriptor());
                            return string;
                        });
                    }
                });
            });
            acn.fields().stream().filter(afn->getDesc().getTargetType().getDescriptor().equals(afn.desc().value())).forEach(afn-> {
                AsmrNodeCapture capture = processsor.refCapture(afn.desc());
                processsor.addWrite(this, capture, ()->{
                    AsmrValueNode node = new AsmrValueNode<>();
                    node.init(getDesc().getReplacingType().getDescriptor());
                    return node;
                });
            });
            for(AsmrMethodNode amn : acn.methods()) {
                final MethodDesc desc = new MethodDesc(amn.desc().value()).replaceMatching(getDesc().getTargetType(), getDesc().getReplacingType());
                if(!amn.desc().value().equals(desc.toDesc())) {
                    AsmrNodeCapture capture = processsor.refCapture(amn.desc());
                    processsor.addWrite(this, capture, ()-> {
                        AsmrValueNode node = new AsmrValueNode<>();
                        node.init(desc.toDesc());
                        return node;
                    });
                }
            }
        });
    }
    
    @Override
    public abstract ObjectReplacementDesc getDesc();
}
