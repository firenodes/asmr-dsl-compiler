package biom4st3r.asmr_compiler.templatetransformers;

import static org.objectweb.asm.Opcodes.ACC_PUBLIC;
import static org.objectweb.asm.Opcodes.ACC_STATIC;
import static org.objectweb.asm.Opcodes.V1_8;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Type;
import org.quiltmc.asmr.processor.AsmrProcessor;
import org.quiltmc.asmr.processor.AsmrTransformer;

import biom4st3r.asmr_compiler.OpcodeMethodVisitor;
import biom4st3r.asmr_compiler.OpcodeMethodVisitor.OpcodeClassVisitor;
import biom4st3r.asmr_compiler.asm_accessed.Description;
import biom4st3r.asmr_compiler.util.Stringhash;

public abstract class TransformerGenerator implements AsmrTransformer {
    
    public abstract Description getDesc();
    
    @SuppressWarnings({"rawtypes","unchecked"})
    public static Class<? extends AsmrTransformer> getTransformerClass(String className, Class<?> superClass, Description desc, Class<?> descriptionClass, AsmrProcessor processor) {
        Type T_CUSTOM_TRANSFORMER = Type.getType(superClass);
        Type T_DESCRIPTION = Type.getType(descriptionClass);
        Type T_SUPER = Type.getType(superClass);

        String fieldname = Stringhash.getHash(desc.getSourceFileName());
        className += fieldname;
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
        OpcodeClassVisitor cv = OpcodeMethodVisitor.newCv(cw);
        cv.visit(V1_8, ACC_PUBLIC, className, null, T_CUSTOM_TRANSFORMER.getInternalName(), null);
        { 
            cv.visitField(ACC_PUBLIC|ACC_STATIC, fieldname, T_DESCRIPTION.getDescriptor(), null, null);
            
            OpcodeMethodVisitor mv = cv.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
            mv.visitCode(); {
                mv
                    .ALOAD(0)
                    .INVOKE_SPECIAL(T_SUPER.getInternalName(), "<init>", "()V")
                    .RETURN()
                    .visitMaxs(10, 10);
            } mv.visitEnd();

            mv = cv.visitMethod(ACC_PUBLIC, "getDesc", "()"+T_DESCRIPTION.getDescriptor(), null, null);
            mv.visitCode(); {
                mv.
                    GET_STATIC(className, fieldname, T_DESCRIPTION.getDescriptor())
                    .ARETURN()
                    .visitMaxs(10, 10); 
            } mv.visitEnd();
        } cv.visitEnd();

        try {
            String[] sec = className.split("/");
            File f = new File(sec[sec.length-1]+".class");
            FileOutputStream stream = new FileOutputStream(f);
            stream.write(cw.toByteArray());
            stream.close();
            Method defineClass = ClassLoader.class.getDeclaredMethod("defineClass", byte[].class, int.class, int.class);
            defineClass.setAccessible(true);
            Class clazz = (Class) defineClass.invoke(Thread.currentThread().getContextClassLoader(), cw.toByteArray(), 0, cw.toByteArray().length);

            clazz.getDeclaredField(fieldname).set(null, desc);
            
            return clazz;
        } catch (IOException | NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }
}
