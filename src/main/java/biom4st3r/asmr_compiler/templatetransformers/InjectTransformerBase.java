package biom4st3r.asmr_compiler.templatetransformers;

import java.util.List;
import java.util.stream.Collectors;

import org.quiltmc.asmr.processor.AsmrProcessor;
import org.quiltmc.asmr.processor.capture.AsmrNodeCapture;
import org.quiltmc.asmr.processor.capture.AsmrSliceCapture;
import org.quiltmc.asmr.processor.tree.AsmrAbstractListNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrInstructionListNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrLineNumberNode;
import org.quiltmc.asmr.processor.tree.member.AsmrMethodNode;
import org.quiltmc.asmr.processor.tree.method.AsmrIndex;

import biom4st3r.asmr_compiler.Compiler.Position;
import biom4st3r.asmr_compiler.asm_accessed.InjectTransformerDesc;
import biom4st3r.asmr_compiler.asm_accessed.MethodTarget;
import biom4st3r.asmr_compiler.insns_v2.Insn;
import biom4st3r.asmr_compiler.util.Lists;

public abstract class InjectTransformerBase extends TransformerGenerator {
    @SuppressWarnings({"unchecked","rawtypes"})
    @Override
    public void read(AsmrProcessor proc) {
        for(String clazz : getDesc().classTargets) {
            proc.withClass(clazz, (acn)-> {
                List<AsmrMethodNode> targetMethods = Lists.newArrayList();
                for(MethodTarget mn : getDesc().methodTargets) {
                    List<AsmrMethodNode> nodes = acn.methods().stream().filter((amn)->mn.matches(amn)).collect(Collectors.toList());
                    targetMethods.addAll(nodes);
                }
                for (AsmrMethodNode amn : targetMethods) {
                    getDesc().matchingInfo.forEach(matchingInfo-> {
                        final List<Integer> insnTargetIndexes = matchingInfo.findMatch(amn);
                        for(int insnIndex : insnTargetIndexes) {
                            final AsmrSliceCapture cap = proc.refCapture(amn.body().instructions(), insnIndex, insnIndex, matchingInfo.position == Position.BEFORE, matchingInfo.position == Position.AFTER);
                            proc.addWrite(this, (AsmrSliceCapture) cap, ()-> {
                                AsmrAbstractListNode list = new AsmrInstructionListNode<>(null);
                                List<Insn> insn = getDesc().getBodyAddition();
                                for(int i = 0; i < getDesc().getBodySize(); i++) {
                                    insn.get(i).accept(amn.body());
                                    AsmrAbstractInsnNode bodyInsn = (insn.get(i)).as(list);
                                    list.addCopy(bodyInsn);
                                }
                                return list; 
                            });
                        }
                        if(getDesc().modifiesLineNumbers) {
                            for(AsmrAbstractInsnNode node : amn.body().instructions()) {
                                int[] prevLine = new int[]{-1};
                                AsmrIndex[] index = new AsmrIndex[]{null};
                                boolean foundInjectedLines = false;
                                if(node instanceof AsmrLineNumberNode) {
                                    AsmrLineNumberNode line = (AsmrLineNumberNode)node;
                                    if(line.line().value().intValue() != -1 && !foundInjectedLines) {
                                        prevLine[0] = line.line().value().intValue();
                                        if(index[0] == null) {
                                            index[0] = line.start().value();
                                        }
                                    } else { // updates injected LineNumbers and replaces linenumbers after inject
                                        proc.addWrite(this, (AsmrNodeCapture)proc.refCapture(node), () -> {
                                            AsmrLineNumberNode lnn = new AsmrLineNumberNode(amn.body().instructions());
                                            lnn.line().init(++prevLine[0]);
                                            lnn.start().init(index[0]);
                                            return lnn;
                                        });
                                    }
                                }
                            }
                        }
                    });
                }
            });
        }
    }

    @Override
    public void apply(AsmrProcessor processor) {
    }


    @Override
    public abstract InjectTransformerDesc getDesc();
}