package biom4st3r.asmr_compiler.asm_accessed;

import java.util.List;
import java.util.Optional;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.quiltmc.asmr.processor.AsmrProcessor;
import org.quiltmc.asmr.processor.AsmrTransformer;
import org.quiltmc.asmr.processor.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrFieldInsnNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrInstructionListNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrMethodInsnNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrNoOperandInsnNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrTypeInsnNode;

import biom4st3r.asmr_compiler.Biom4st3rCompilerError;
import biom4st3r.asmr_compiler.Compiler.Pair;
import biom4st3r.asmr_compiler.templatetransformers.ReplaceObjTransformerBase;
import biom4st3r.asmr_compiler.templatetransformers.TransformerGenerator;
import biom4st3r.asmr_compiler.util.Lists;

@SuppressWarnings({"rawtypes", "unchecked"})
public class ObjectReplacementDesc implements Description {
    public List<Pair<MethodTarget,MethodTarget>> targets = Lists.newArrayList();
    public String targetClass;
    public String replacingClass;
    public String sourceFile;

    public boolean isCanadate(AsmrAbstractInsnNode node) {
        if(node instanceof AsmrMethodInsnNode && ((AsmrMethodInsnNode)node).owner().value().equals(targetClass)) {
            return true;
        } else if(node instanceof AsmrTypeInsnNode && ((AsmrTypeInsnNode)node).desc().value().equals(targetClass)) {
            return true;
        } else if(node instanceof AsmrFieldInsnNode && ((AsmrFieldInsnNode)node).desc().value().equals(this.getTargetType().getDescriptor())) {
            return true;
        }
        return false;
    }

    public Type getTargetType() {
        return Type.getObjectType(targetClass);
    }
    public Type getReplacingType() {
        return Type.getObjectType(replacingClass);
    }

    public AsmrInstructionListNode getReplacingInsn(AsmrMethodInsnNode node) {
        Optional<Pair<MethodTarget, MethodTarget>> optional = targets.stream().filter((pair)->pair.left.matches(node)).findFirst();
        if(!optional.isPresent()) {
            throw new Biom4st3rCompilerError("No matching replacer for %s.%s%s", node.owner().value(),node.name().value(),node.desc().value());
        }
        MethodTarget target = optional.get().left;
        MethodTarget replacer = optional.get().right;
        Type[] targetArgs = Type.getArgumentTypes(target.desc);
        Type[] replacerArgs = Type.getArgumentTypes(replacer.desc);

        AsmrInstructionListNode list = new AsmrInstructionListNode<>();

        if(targetArgs.length < replacerArgs.length) {
            throw new Biom4st3rCompilerError("Not enough arguments in %s.%s%s or %s.%s%s", node.owner().value(),node.name().value(),node.desc().value(), replacer.owner, replacer.name, replacer.desc);
        } else if(targetArgs.length > replacerArgs.length) {
            // TODO Handle POP2 for long and double
            AsmrNoOperandInsnNode insn = new AsmrNoOperandInsnNode(list); // TODO Make this smarter
            insn.opcode().init(Opcodes.POP);
            for(int i = 0; i < targetArgs.length - replacerArgs.length; i++) {
                list.addCopy(insn);
            }
        }
        AsmrMethodInsnNode newInvoke = new AsmrMethodInsnNode();
        newInvoke.opcode().init(node.opcode().value());
        newInvoke.name().init(replacer.name);
        newInvoke.owner().init(replacer.owner);
        newInvoke.desc().init(replacer.desc);
        newInvoke.itf().init(node.opcode().value() == Opcodes.INVOKEINTERFACE);
        list.addCopy(newInvoke);
        return list;
    }

    @Override
    public String getSourceFileName() {
        return this.sourceFile;
    }

    @Override
    public Class<? extends AsmrTransformer> buildTransformerClass(AsmrProcessor processor) {
        return TransformerGenerator.getTransformerClass("biom4st3r/asm/injector/ObjectReplacementTransformer_"+sourceFile, ReplaceObjTransformerBase.class, this, this.getClass(), processor);
    }

    @Override
    public void setSourceFileName(String s) {
        this.sourceFile = s;        
    }
}
