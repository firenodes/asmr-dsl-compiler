package biom4st3r.asmr_compiler.asm_accessed;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.quiltmc.asmr.processor.AsmrProcessor;
import org.quiltmc.asmr.processor.AsmrTransformer;

import biom4st3r.asmr_compiler.ConverterDoDad;
import biom4st3r.asmr_compiler.insns_v2.Insn;
import biom4st3r.asmr_compiler.templatetransformers.InjectTransformerBase;
import biom4st3r.asmr_compiler.templatetransformers.TransformerGenerator;
import biom4st3r.asmr_compiler.util.Lists;
import biom4st3r.asmr_compiler.util.TextBuilder;

public class InjectTransformerDesc implements Description {
    public static int INDEX = 0;
    public final int index;
    public String sourceFile = "";
    public final List<String> classTargets = Lists.newArrayList();
    public final List<MatchingInfo> matchingInfo = Lists.newArrayList();
    public final List<MethodTarget> methodTargets = Lists.newArrayList();
    private final List<String> bodyAddition = Lists.newArrayList();
    public boolean modifiesLineNumbers = false;

    public List<Insn> getBodyAddition() {
        return bodyAddition.stream().map((s)->ConverterDoDad.getNodeFromString(s.split("[\t ]"))).collect(Collectors.toList()); // hax
    }

    public void addInsn(String s) {
        if(s.split(" ")[0].equals("LINE")) {
            this.modifiesLineNumbers = true;
        }
        bodyAddition.add(s);
    }

    public String internaGetInsn(int i) {
        return bodyAddition.get(i);
    }

    public int getBodySize() {
        return bodyAddition.size();
    }

    public InjectTransformerDesc() {
        this.index = INDEX;
        INDEX++;
    }

    public InjectTransformerDesc(int i) {
        this.index = 0;
    }

    public void addMethod(String[] info) {
        this.methodTargets.add(new MethodTarget(info));
    }

    public String[] internalGetMethodTarget(int i) {
        return methodTargets.get(i).internalGet();
    }
    
    
    @Override
    public String toString() {
        TextBuilder builder = new TextBuilder();
        builder.append("Class Targets:").newLine();
        for(String s : classTargets) {
            builder.append("    ").append(s).newLine();
        }
        builder.append("Inject").newLine();
        Iterator<MatchingInfo> iter = matchingInfo.iterator();
        for(MatchingInfo info = null; iter.hasNext();) {
            info = iter.next();

            builder.append(info.position.name()).newLine();
            

            for(Insn node : info.getInsns().collect(Collectors.toSet())) {
                builder.append("    ").append(node.toString()).newLine();
            }
            if(iter.hasNext()) builder.append("\n    or").newLine();
        }
        builder.append("In methods matching:").newLine();
        for(MethodTarget node : methodTargets) {
            builder.append("    ").append(node.name).append(node.desc).newLine();
        }
        builder.append("Body:").newLine();
        for(Insn insn : this.getBodyAddition()) {
            builder.append("    ").append(insn.toString()).newLine();
        }
        return builder.toString();
    }

    @Override
    public String getSourceFileName() {
        return this.sourceFile;
    }

    @Override
    public Class<? extends AsmrTransformer> buildTransformerClass(AsmrProcessor processor) {
        return TransformerGenerator.getTransformerClass("biom4st3r/asm/injector/Injectiontransformer_"+sourceFile, InjectTransformerBase.class, this, this.getClass(), processor);
    }

    @Override
    public void setSourceFileName(String s) {
        this.sourceFile = s;       
    }

}