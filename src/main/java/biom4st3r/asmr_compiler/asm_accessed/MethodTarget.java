package biom4st3r.asmr_compiler.asm_accessed;

import biom4st3r.asmr_compiler.Biom4st3rCompilerError;

import org.quiltmc.asmr.processor.tree.AsmrNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrMethodInsnNode;
import org.quiltmc.asmr.processor.tree.member.AsmrClassNode;
import org.quiltmc.asmr.processor.tree.member.AsmrMethodNode;

public class MethodTarget {
    final String desc;
    final String name;
    final String owner;
    private static final String skip = "#SKIP";
    public MethodTarget(String[] args) {
        if(args.length == 4) {
            this.owner = args[1];
            this.name = args[2];
            this.desc = args[3];
        } else {
            if(args[0].equals("method")) throw new Biom4st3rCompilerError("Malformed Method instruction. Proper format 'method theMethodName (theDesc)V'");
            this.owner = args[0];
            this.name = args[1];
            this.desc = args[2];
        }
    }
    
    public boolean matches(AsmrMethodNode node) {
        boolean 
            matchname = node.name().value().equals(name) || this.skipName(),
            matchdesc = node.desc().value().equals(desc) || this.skipDesc(),
            matchowner = (node.parent() != null && ((AsmrClassNode)node.parent().parent()).name().value().equals(owner)) || this.skipOwner();
        return matchdesc && matchname && matchowner;
    }    
    public boolean matches(AsmrMethodInsnNode node) {
        boolean 
            matchname = node.name().value().equals(name) || this.skipName(),
            matchdesc = node.desc().value().equals(desc) || this.skipDesc(),
            matchowner = node.owner().value().equals(owner) || this.skipOwner();
        return matchdesc && matchname && matchowner;
    }

    private static <T extends AsmrNode<?>> T parentOf(Class<T> clazz, AsmrNode<?> target) {
        while(target != null) {
            target = target.parent();
            if(clazz.isInstance(target)) return (T) target;
        }
        return null;
    } 

    private boolean skipName() {
        return this.name.equals(skip);
    }
    private boolean skipDesc() {
        return this.desc.equals(skip);
    }
    private boolean skipOwner() {
        return this.owner.equals(skip);
    }
    public String[] internalGet() {
        return new String[]{owner,name,desc};
    }
}
