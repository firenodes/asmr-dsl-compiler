package biom4st3r.asmr_compiler.asm_accessed;

import org.quiltmc.asmr.processor.AsmrProcessor;
import org.quiltmc.asmr.processor.AsmrTransformer;

public interface Description {
    String getSourceFileName();
    void setSourceFileName(String s);
	Class<? extends AsmrTransformer> buildTransformerClass(AsmrProcessor processor);
}
