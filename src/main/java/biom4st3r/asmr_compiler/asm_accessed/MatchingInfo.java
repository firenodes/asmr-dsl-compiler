package biom4st3r.asmr_compiler.asm_accessed;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import biom4st3r.asmr_compiler.Biom4st3rCompilerError;
import biom4st3r.asmr_compiler.Compiler.Position;
import biom4st3r.asmr_compiler.ConverterDoDad;
import biom4st3r.asmr_compiler.insns_v2.Insn;
import biom4st3r.asmr_compiler.util.Lists;
import org.quiltmc.asmr.processor.tree.AsmrAbstractListNode;
import org.quiltmc.asmr.processor.tree.AsmrNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.processor.tree.member.AsmrMethodNode;

public class MatchingInfo {
    final public List<String[]> opcodeList = Lists.newArrayList();
    public int ordinal = -1;
    public Position position = Position.AFTER;

    public Stream<Insn> getInsns() {
        return opcodeList.stream().map((string)->ConverterDoDad.getNodeFromString(string));
    }

    public Insn getInsn(int i) {
        return ConverterDoDad.getNodeFromString(opcodeList.get(i));
    }

    public AsmrAbstractInsnNode<?> getInsnAsmr(int i, AsmrNode<?> parent) {
        return getInsn(i).as(parent);
    }

    private static int indexOf(AsmrNode<?> node , AsmrAbstractListNode<?,?> list) {
        for(int i = 0; i < list.size(); i++) {
            if(list.get(i) == node) {
                return i;
            } 
        }
        return -1;
    }

    @SuppressWarnings({"unchecked"})
    public List<Integer> findMatch(AsmrMethodNode node) {
        List<Integer> indices = Lists.newArrayList();
        Iterator<AsmrAbstractInsnNode<? extends AsmrNode<?>>> iter = (Iterator<AsmrAbstractInsnNode<? extends AsmrNode<?>>>) node.body().instructions().iterator();
        while(iter.hasNext()) {
            AsmrAbstractInsnNode<?> insn = iter.next();
            Iterator<Insn> matchInsns = getInsns().iterator();
            while(matchInsns.hasNext()) {
                Insn matchInsn = matchInsns.next(); // Trust me
                if(!matchInsn.matches(insn)) break;
                if(!matchInsns.hasNext()) {
                    indices.add(indexOf(insn, node.body().instructions()) - (position == Position.AFTER ? 0 : opcodeList.size()));
                }
            }
        }
        try {
            return ordinal == -1 ? indices : Lists.newArrayList(indices.get(ordinal));
        } catch(IndexOutOfBoundsException e) {
            throw new Biom4st3rCompilerError("Matching ordinal incorrect: %s", opcodeList);
        }
    }
}