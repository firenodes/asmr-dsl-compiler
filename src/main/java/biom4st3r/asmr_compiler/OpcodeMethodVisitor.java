package biom4st3r.asmr_compiler;

import java.util.function.Consumer;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

public class OpcodeMethodVisitor extends MethodVisitor {

    public OpcodeMethodVisitor(int api, MethodVisitor methodVisitor) {
        super(api, methodVisitor);
    }
    public OpcodeMethodVisitor(int api) {
        super(api);
    }

    @Override
    public void visitVarInsn(int opcode, int var) {
        // if(random.nextInt(3) == 0) this.NOP();
        super.visitVarInsn(opcode, var);
    }

    @Override
    public void visitInsn(int opcode) {
        // if(random.nextInt(3) == 0) this.NOP();
        super.visitInsn(opcode);
    }

    public OpcodeMethodVisitor ASTORE(int index) {
        this.visitVarInsn(Opcodes.ASTORE, index);
        return this;
    }
    public OpcodeMethodVisitor ISTORE(int index) {
        this.visitVarInsn(Opcodes.ISTORE, index);
        return this;
    }
    public OpcodeMethodVisitor DSTORE(int index) {
        this.visitVarInsn(Opcodes.DSTORE, index);
        return this;
    }
    public OpcodeMethodVisitor FSTORE(int index) {
        this.visitVarInsn(Opcodes.FSTORE, index);
        return this;
    }
    public OpcodeMethodVisitor LSTORE(int index) {
        this.visitVarInsn(Opcodes.LSTORE, index);
        return this;
    }

    public OpcodeMethodVisitor AASTORE() {
        this.visitInsn(Opcodes.AASTORE);
        return this;
    }
    public OpcodeMethodVisitor BASTORE() {
        this.visitInsn(Opcodes.BASTORE);
        return this;
    }
    public OpcodeMethodVisitor CASTORE() {
        this.visitInsn(Opcodes.CASTORE);
        return this;
    }
    public OpcodeMethodVisitor IASTORE() {
        this.visitInsn(Opcodes.IASTORE);
        return this;
    }
    public OpcodeMethodVisitor LASTORE() {
        this.visitInsn(Opcodes.LASTORE);
        return this;
    }
    public OpcodeMethodVisitor FASTORE() {
        this.visitInsn(Opcodes.FASTORE);
        return this;
    }
    public OpcodeMethodVisitor DASTORE() {
        this.visitInsn(Opcodes.DASTORE);
        return this;
    }

    public OpcodeMethodVisitor DUP() {
        this.visitInsn(Opcodes.DUP);
        return this;
    }

    public OpcodeMethodVisitor ALOAD(int index) {
        this.visitVarInsn(Opcodes.ALOAD, index);
        return this;
    }
    public OpcodeMethodVisitor ILOAD(int index) {
        this.visitVarInsn(Opcodes.ILOAD, index);
        return this;
    }
    public OpcodeMethodVisitor DLOAD(int index) {
        this.visitVarInsn(Opcodes.DLOAD, index);
        return this;
    }
    public OpcodeMethodVisitor FLOAD(int index) {
        this.visitVarInsn(Opcodes.FLOAD, index);
        return this;
    }
    public OpcodeMethodVisitor LLOAD(int index) {
        this.visitVarInsn(Opcodes.LLOAD, index);
        return this;
    }

    public OpcodeMethodVisitor AALOAD(int index) {
        this.visitVarInsn(Opcodes.AALOAD, index);
        return this;
    }
    public OpcodeMethodVisitor BALOAD(int index) {
        this.visitVarInsn(Opcodes.BALOAD, index);
        return this;
    }
    public OpcodeMethodVisitor CALOAD(int index) {
        this.visitVarInsn(Opcodes.CALOAD, index);
        return this;
    }
    public OpcodeMethodVisitor IALOAD(int index) {
        this.visitVarInsn(Opcodes.IALOAD, index);
        return this;
    }
    public OpcodeMethodVisitor LALOAD(int index) {
        this.visitVarInsn(Opcodes.LALOAD, index);
        return this;
    }
    public OpcodeMethodVisitor FALOAD(int index) {
        this.visitVarInsn(Opcodes.FALOAD, index);
        return this;
    }
    public OpcodeMethodVisitor DALOAD(int index) {
        this.visitVarInsn(Opcodes.DALOAD, index);
        return this;
    }

    public OpcodeMethodVisitor GET_FIELD(String owner, String fieldName, String desc) {
        this.visitFieldInsn(Opcodes.GETFIELD, owner, fieldName, desc);
        return this;
    }
    public OpcodeMethodVisitor GET_STATIC(String owner, String fieldName, String desc) {
        this.visitFieldInsn(Opcodes.GETSTATIC, owner, fieldName, desc);
        return this;
    }
    public OpcodeMethodVisitor GET_FIELD(String owner, String fieldName, Class<?> desc) {
        this.GET_FIELD(owner, fieldName, org.objectweb.asm.Type.getDescriptor(desc));
        return this;
    }

    public OpcodeMethodVisitor PUT_FIELD(String owner, String fieldName, String desc) {
        this.visitFieldInsn(Opcodes.PUTFIELD, owner, fieldName, desc);
        return this;
    }
    public OpcodeMethodVisitor PUT_FIELD(String owner, String fieldName, Class<?> desc) {
        this.PUT_FIELD(owner, fieldName, org.objectweb.asm.Type.getDescriptor(desc));
        return this;
    }

    public OpcodeMethodVisitor RETURN() {
        this.visitInsn(Opcodes.RETURN);
        return this;
    }
    public OpcodeMethodVisitor IRETURN() {
        this.visitInsn(Opcodes.IRETURN);
        return this;
    }
    public OpcodeMethodVisitor DRETURN() {
        this.visitInsn(Opcodes.DRETURN);
        return this;
    }
    public OpcodeMethodVisitor FRETURN() {
        this.visitInsn(Opcodes.FRETURN);
        return this;
    }
    public OpcodeMethodVisitor LRETURN() {
        this.visitInsn(Opcodes.LRETURN);
        return this;
    }
    public OpcodeMethodVisitor ARETURN() {
        this.visitInsn(Opcodes.ARETURN);
        return this;
    }

    public OpcodeMethodVisitor ARRAY_LENGTH() {
        this.visitInsn(Opcodes.ARRAYLENGTH);
        return this;
    }

    public OpcodeMethodVisitor NEW_ARRAY(int arrayType) {
        this.visitIntInsn(Opcodes.NEWARRAY, arrayType);
        return this;
    }

    public OpcodeMethodVisitor ANEW_ARRAY(String interalName) {
        this.visitTypeInsn(Opcodes.ANEWARRAY, interalName);
        return this;
    }
    public OpcodeMethodVisitor CHECKCAST(String interalName) {
        this.visitTypeInsn(Opcodes.CHECKCAST, interalName);
        return this;
    }
    public OpcodeMethodVisitor INSTANCEOF(String internalName) {
        this.visitTypeInsn(Opcodes.INSTANCEOF, internalName);
        return this;
    }
    public OpcodeMethodVisitor NEW(String internalName) {
        this.visitTypeInsn(Opcodes.NEW, internalName);
        return this;
    }

    public OpcodeMethodVisitor NOP() {
        this.visitInsn(Opcodes.NOP);
        return this;
    }

    public OpcodeMethodVisitor IADD() {
        this.visitInsn(Opcodes.IADD);
        return this;
    }
    public OpcodeMethodVisitor ISUB() {
        this.visitInsn(Opcodes.ISUB);
        return this;
    }
    public OpcodeMethodVisitor IMUL() {
        this.visitInsn(Opcodes.IMUL);
        return this;
    }
    public OpcodeMethodVisitor IDIV() {
        this.visitInsn(Opcodes.IDIV);
        return this;
    }
    public OpcodeMethodVisitor POP() {
        this.visitInsn(Opcodes.POP);
        return this;
    }
    public OpcodeMethodVisitor SWAP() {
        this.visitInsn(Opcodes.SWAP);
        return this;
    }
    public OpcodeMethodVisitor LDC(String s) {
        this.visitLdcInsn(s);
        return this;
    }
    public OpcodeMethodVisitor LDC(Integer s) {
        this.visitLdcInsn(s);
        return this;
    }
    public OpcodeMethodVisitor LDC(Long s) {
        this.visitLdcInsn(s);
        return this;
    }
    public OpcodeMethodVisitor LDC(Double s) {
        this.visitLdcInsn(s);
        return this;
    }
    public OpcodeMethodVisitor LDC(Float s) {
        this.visitLdcInsn(s);
        return this;
    }
    public OpcodeMethodVisitor INVOKE_VIRTUAL(String internalName, String name, String desc) {
        this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, internalName, name, desc, false);
        return this;
    }
    public OpcodeMethodVisitor INVOKE_STATIC(String internalName, String name, String desc) {
        this.visitMethodInsn(Opcodes.INVOKESTATIC, internalName, name, desc, false);
        return this;
    }
    public OpcodeMethodVisitor INVOKE_SPECIAL(String internalName, String name, String desc) {
        this.visitMethodInsn(Opcodes.INVOKESPECIAL, internalName, name, desc, false);
        return this;
    }
    public OpcodeMethodVisitor INVOKE_INTERFACE(String internalName, String name, String desc) {
        this.visitMethodInsn(Opcodes.INVOKEINTERFACE, internalName, name, desc, true);
        return this;
    }

    // ~~~~~~~~Custom~~~~~~~~

    /**
     * Stack: () -> Object
     * @param internalName
     * @param desc
     * @param loadArgs
     */
    public OpcodeMethodVisitor _CTOR(String internalName, String desc, Consumer<OpcodeMethodVisitor> loadArgs) {
        this.NEW(internalName);
        this.DUP();
        loadArgs.accept(this);
        this.INVOKE_SPECIAL(internalName, "<init>", desc);
        return this;
    }
    public OpcodeMethodVisitor _LOAD_NUM(Number n) {
        if(n instanceof Float) {
            this._LOAD_F((Float)n);
        }
        else if(n instanceof Integer) {
            this._LOAD_I((Integer)n);
        }
        else if(n instanceof Double) {
            this._LOAD_D((Double)n);
        }
        else if(n instanceof Long) {
            this._LOAD_J((Long)n);
        } else {
            throw new Biom4st3rCompilerError("_LOAD_NUM Failed? %s", n);
        }
        return this;
    }
    public OpcodeMethodVisitor _LOAD_J(long val) {
        if(val == 0L) {
            this.visitInsn (Opcodes.LCONST_0);
        } else if(val == 1L) {
            this.visitInsn (Opcodes.LCONST_1);
        } else if(val < Integer.MIN_VALUE || val > Integer.MAX_VALUE) {
            this.visitLdcInsn(val);
        }
        else {
            this._LOAD_I((int) val);
            this.visitInsn(Opcodes.I2L); // is this needed?
        }
        return this;
    }
    public OpcodeMethodVisitor _LOAD_D(double val) {
        if(val == -1) {
            this.visitInsn(Opcodes.DCONST_0);
        }
        else if (val == 0) {
            this.visitInsn(Opcodes.DCONST_1);
        }
        else {
            this.LDC(val);
        }
        return this;
    }
    public OpcodeMethodVisitor _LOAD_F(float val) {
        if(val == -1) {
            this.visitInsn(Opcodes.FCONST_0);
        }
        else if (val == 0) {
            this.visitInsn(Opcodes.FCONST_1);
        }
        else if (val == 0) {
            this.visitInsn(Opcodes.FCONST_2);
        }
        else {
            this.LDC(val);
        }
        return this;
    }
    public OpcodeMethodVisitor _LOAD_I(int val) {
        if(val == -1) {
            this.visitInsn(Opcodes.ICONST_M1);
        }
        else if (val == 0) {
            this.visitInsn(Opcodes.ICONST_0);
        }
        else if (val == 1) {
            this.visitInsn(Opcodes.ICONST_1);
        }
        else if (val == 2) {
            this.visitInsn(Opcodes.ICONST_2);
        }
        else if (val == 3) {
            this.visitInsn(Opcodes.ICONST_3);
        }
        else if (val == 4) {
            this.visitInsn(Opcodes.ICONST_4);
        }
        else if (val == 5) {
            this.visitInsn(Opcodes.ICONST_5);
        }
        else if (val <= Byte.MAX_VALUE && val >= Byte.MIN_VALUE) {
            this.visitIntInsn(Opcodes.BIPUSH, val);
        }
        else if (val <= Short.MAX_VALUE && val >= Short.MIN_VALUE) {
            this.visitIntInsn(Opcodes.SIPUSH, val);
        } else {
            this.visitLdcInsn(val);
        }
        return this;
    }

    /**
     * Stack () -> String[array.length]{array.elements...}
     * @param array
     */
    public OpcodeMethodVisitor _ANEW_String_ARRAY(String[] array) {
        Type T_Type = Type.getType(String.class);
        this._LOAD_I(array.length);
        this.ANEW_ARRAY(T_Type.getInternalName());
        for (int i = 0; i < array.length; i++) {
            this.DUP();
            this._LOAD_I(i);
            this.LDC(array[i]);
            this.AASTORE();
        }
        return this;
    }

    public static class OpcodeClassVisitor extends ClassVisitor {
        public OpcodeClassVisitor(int api, ClassVisitor classVisitor) {
            super(api, classVisitor);
        }
        public OpcodeClassVisitor(int api) {
            super(api);
        }

        @Override
        public void visit(int version, int access, String name, String signature, String superName,
                String[] interfaces) {
            super.visit(Opcodes.V1_8, access, name, signature, superName, interfaces);
        }

        @Override
        public OpcodeMethodVisitor visitMethod(int access, String name, String descriptor, String signature,
                String[] exceptions) {
            return new OpcodeMethodVisitor(api, super.visitMethod(access, name, descriptor, signature, exceptions));
        }
    }

    public static OpcodeClassVisitor newCv(ClassVisitor cv) {
        return new OpcodeClassVisitor(Opcodes.ASM8, cv);
    }
}
