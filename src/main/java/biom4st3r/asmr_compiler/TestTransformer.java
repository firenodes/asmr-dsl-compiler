package biom4st3r.asmr_compiler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.util.TraceClassVisitor;
import org.quiltmc.asmr.processor.AsmrClassWriter;
import org.quiltmc.asmr.processor.AsmrPlatform;
import org.quiltmc.asmr.processor.AsmrProcessor;
import org.quiltmc.asmr.processor.AsmrTransformer;
import org.quiltmc.asmr.processor.capture.AsmrSliceCapture;
import org.quiltmc.asmr.processor.tree.AsmrAbstractListNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrFieldInsnNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrInstructionListNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrMethodInsnNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrNoOperandInsnNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrVarInsnNode;
import org.quiltmc.asmr.processor.tree.member.AsmrClassNode;
import org.quiltmc.asmr.processor.tree.member.AsmrMethodNode;

public class TestTransformer {
    public static void print(Object o) {
        System.out.println(o);
    }
    public static class TestTransformerr implements AsmrTransformer {

        @Override
        public void apply(AsmrProcessor arg0) {
            
        }

        @SuppressWarnings({"unchecked","rawtypes"})
        @Override
        public void read(AsmrProcessor processor) {
            processor.withClass("net/minecraft/server/PlayerManager", (acn)-> {
                AsmrMethodNode amn = acn.methods().stream().filter((anm)->anm.name().value().equals("onPlayerConnect")).findFirst().get();
                int insnIndex = -1;
                for (int i = 0; i < amn.body().instructions().size(); i++) {
                    AsmrAbstractInsnNode node = amn.body().instructions().get(i);
                    if(node instanceof AsmrMethodInsnNode) {
                        if(((AsmrMethodInsnNode)node).name().value().equals("sendPacket")) {
                            insnIndex = i;
                            break;
                        }
                    }
                }
                final AsmrSliceCapture cap = processor.refCapture(amn.body().instructions(), insnIndex, insnIndex, true, false);
                processor.addWrite(this, (AsmrSliceCapture)cap, ()-> {
                    AsmrAbstractListNode list = new AsmrInstructionListNode<>(null);

                    AsmrNoOperandInsnNode noper = new AsmrNoOperandInsnNode();
                    noper.opcode().init(Opcodes.ACONST_NULL);
                    list.addCopy(noper);
                    
                    AsmrVarInsnNode varNode = new AsmrVarInsnNode();
                    varNode.opcode().init(Opcodes.ASTORE);
                    varNode.varIndex().init(amn.body().localIndexes().get(0).value());
                    list.addCopy(varNode);

                    AsmrFieldInsnNode fin = new AsmrFieldInsnNode(list);
                    fin.opcode().init(Opcodes.GETSTATIC);
                    fin.owner().init("java/lang/System");
                    fin.name().init("out");
                    fin.desc().init(Type.getDescriptor(PrintStream.class));
                    list.addCopy(fin);

                    AsmrNoOperandInsnNode neg1 = new AsmrNoOperandInsnNode(list);
                    neg1.opcode().init(Opcodes.ICONST_5);
                    list.addCopy(neg1);

                    AsmrMethodInsnNode min = new AsmrMethodInsnNode(list);
                    min.opcode().init(Opcodes.INVOKEVIRTUAL);
                    min.owner().init("java/io/PrintStream");
                    min.name().init("println");
                    min.desc().init("(I)V");
                    min.itf().init(false);
                    list.addCopy(min);
                    
                    return list;
                });
            });
        }

    }

    private static byte[] findClassBytes(Class<?> clazz) {
		Path sourceLocation;
		try {
			sourceLocation = Paths.get(clazz.getProtectionDomain().getCodeSource().getLocation().toURI());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			throw new NoClassDefFoundError(clazz.getName());
		}

		byte[] bytes;
		try {
			if (Files.isDirectory(sourceLocation)) {
				Path classFile = sourceLocation.resolve(clazz.getName().replace('.', File.separatorChar) + ".class");
				bytes = Files.readAllBytes(classFile);
			} else {
				try (FileSystem fs = FileSystems.newFileSystem(sourceLocation, (ClassLoader) null);) {
					Path jarEntry = fs.getPath(clazz.getName().replace('.', '/') + ".class");
					bytes = Files.readAllBytes(jarEntry);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new NoClassDefFoundError(clazz.getName());
		}

		return bytes;
	}

    private static AsmrProcessor getProcessor() {
        AsmrPlatform plateform = new AsmrPlatform() {
            @Override
            public byte[] getClassBytecode(String arg0) throws ClassNotFoundException {
                return findClassBytes(Class.forName(arg0.replace("/", ".")));
            }
        };
        AsmrProcessor processor = new AsmrProcessor(plateform);
        try {
            String className = "net/minecraft/server/PlayerManager";
            processor.addClass(className, plateform.getClassBytecode(className));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        
        return processor;
    }

	private static Class<?> defineClass(AsmrProcessor processor, AsmrClassNode classNode) {
		String className = classNode.name().value().replace('/', '.');

		ClassWriter writer = new AsmrClassWriter(processor);
		classNode.accept(writer);
		byte[] bytecode = writer.toByteArray();

		ClassLoader classLoader = new ClassLoader(Thread.currentThread().getContextClassLoader()) {
			private Class<?> customClass = null;

			@Override
			protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
				Class<?> loadedClass = findLoadedClass(name);
				if (loadedClass == null) {
					try {
						loadedClass = findClass(name);
					} catch (ClassNotFoundException ignore) {
						return super.loadClass(name, resolve);
					}
				}

				if (resolve) {
					resolveClass(loadedClass);
				}
				return loadedClass;
			}

			@Override
			protected Class<?> findClass(String name) throws ClassNotFoundException {
				if (className.equals(name)) {
					if (customClass == null) {
						customClass = defineClass(className, bytecode, 0, bytecode.length);
					}
					return customClass;
				}
				return super.findClass(name);
			}
		};
		try {
			return classLoader.loadClass(className);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

    @SuppressWarnings({"unchecked","rawtypes","deprecation"})
    public static void main(String args[]) {
        AsmrProcessor processor = getProcessor();
        processor.addTransformer(TestTransformerr.class);
        processor.process();
        try {
            AsmrClassWriter cw = new AsmrClassWriter(processor);

            PrintWriter pw = null;
            try { 
                pw = new PrintWriter(new File( "ohshit.txt"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            TraceClassVisitor tcv = new TraceClassVisitor(cw, pw);

            AsmrClassNode acn = processor.findClassImmediately("net/minecraft/server/PlayerManager");
            acn.accept(tcv);
            System.out.println(pw.toString());
            File f = new File("cww.class");
            FileOutputStream stream = new FileOutputStream(f);
            stream.write(cw.toByteArray());
            stream.close();
            Method defineClass = ClassLoader.class.getDeclaredMethod("defineClass", byte[].class, int.class, int.class);
            defineClass.setAccessible(true);

            Class clazz = defineClass(processor, acn);
            Object pm =  clazz.newInstance();
            clazz.getDeclaredMethod("onPlayerConnect").invoke(pm);
        } catch (Throwable t) {
            System.out.println("Failed ClassLoad");
            t.printStackTrace();
        }
    }
}
