package biom4st3r.asmr_compiler;

import org.objectweb.asm.Opcodes;
import org.quiltmc.asmr.processor.tree.AsmrValueListNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrFieldInsnNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrInstructionListNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrIntInsnNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrLdcInsnNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrNoOperandInsnNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrTypeInsnNode;
import org.quiltmc.asmr.processor.tree.insn.AsmrVarInsnNode;
import org.quiltmc.asmr.processor.tree.method.AsmrIndex;
import org.quiltmc.asmr.processor.tree.method.AsmrMethodBodyNode;

@SuppressWarnings({"rawtypes","unchecked"})
public class AsmrOpcodeListBuilder {

    private final AsmrValueListNode<AsmrIndex> indexes;


    public AsmrOpcodeListBuilder(AsmrMethodBodyNode indexes) {
        this.indexes = indexes.localIndexes();    
    }

    AsmrInstructionListNode list = new AsmrInstructionListNode<>();
    
    public AsmrInstructionListNode<? extends AsmrAbstractInsnNode<?>> finish() {
        return list;
    }

    public void ASTORE(int index) {
        AsmrVarInsnNode node = new AsmrVarInsnNode(list);
        node.opcode().init(Opcodes.ASTORE);
        node.varIndex().init(indexes.get(index).value());
        list.addCopy(node);
    }
    public void ISTORE(int index) {
        AsmrVarInsnNode node = new AsmrVarInsnNode(list);
        node.opcode().init(Opcodes.ISTORE);
        node.varIndex().init(indexes.get(index).value());
        list.addCopy(node);
    }
    public void DSTORE(int index) {
        AsmrVarInsnNode node = new AsmrVarInsnNode(list);
        node.opcode().init(Opcodes.DSTORE);
        node.varIndex().init(indexes.get(index).value());
        list.addCopy(node);
    }
    public void FSTORE(int index) {
        AsmrVarInsnNode node = new AsmrVarInsnNode(list);
        node.opcode().init(Opcodes.FSTORE);
        node.varIndex().init(indexes.get(index).value());
        list.addCopy(node);
    }
    public void LSTORE(int index) {
        AsmrVarInsnNode node = new AsmrVarInsnNode(list);
        node.opcode().init(Opcodes.LSTORE);
        node.varIndex().init(indexes.get(index).value());
        list.addCopy(node);
    }

    public void AASTORE() {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.AASTORE);
        list.addCopy(node);
    }
    public void BASTORE() {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.BASTORE);
        list.addCopy(node);
    }
    public void CASTORE() {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.CASTORE);
        list.addCopy(node);
    }
    public void IASTORE() {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.IASTORE);
        list.addCopy(node);
    }
    public void LASTORE() {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.LASTORE);
        list.addCopy(node);
    }
    public void FASTORE() {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.FASTORE);
        list.addCopy(node);
    }
    public void DASTORE() {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.DASTORE);
        list.addCopy(node);
    }

    public void DUP() {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.DUP);
        list.addCopy(node);
    }

    public void ALOAD(int index) {
        AsmrVarInsnNode node = new AsmrVarInsnNode(list);
        node.opcode().init(Opcodes.ALOAD);
        node.varIndex().init(indexes.get(index).value());
        list.addCopy(node);
    }
    public void ILOAD(int index) {
        AsmrVarInsnNode node = new AsmrVarInsnNode(list);
        node.opcode().init(Opcodes.ILOAD);
        node.varIndex().init(indexes.get(index).value());
        list.addCopy(node);
    }
    public void DLOAD(int index) {
        AsmrVarInsnNode node = new AsmrVarInsnNode(list);
        node.opcode().init(Opcodes.DLOAD);
        node.varIndex().init(indexes.get(index).value());
        list.addCopy(node);
    }
    public void FLOAD(int index) {
        AsmrVarInsnNode node = new AsmrVarInsnNode(list);
        node.opcode().init(Opcodes.FLOAD);
        node.varIndex().init(indexes.get(index).value());
        list.addCopy(node);
    }
    public void LLOAD(int index) {
        AsmrVarInsnNode node = new AsmrVarInsnNode(list);
        node.opcode().init(Opcodes.LLOAD);
        node.varIndex().init(indexes.get(index).value());
        list.addCopy(node);
    }

    public void AALOAD(int index) {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.AALOAD);
        list.addCopy(node);
    }
    public void BALOAD(int index) {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.BALOAD);
        list.addCopy(node);
    }
    public void CALOAD(int index) {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.CALOAD);
        list.addCopy(node);
    }
    public void IALOAD(int index) {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.IALOAD);
        list.addCopy(node);
    }
    public void LALOAD(int index) {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.LALOAD);
        list.addCopy(node);
    }
    public void FALOAD(int index) {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.FALOAD);
        list.addCopy(node);
    }
    public void DALOAD(int index) {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.DALOAD);
        list.addCopy(node);
    }

    public void GET_FIELD(String owner, String fieldName, String desc) {
        AsmrFieldInsnNode node = new AsmrFieldInsnNode(list);
        node.opcode().init(Opcodes.GETFIELD);
        node.name().init(fieldName);
        node.desc().init(desc);
        node.owner().init(owner);
        list.addCopy(node);
    }

    public void PUT_FIELD(String owner, String fieldName, String desc) {
        AsmrFieldInsnNode node = new AsmrFieldInsnNode(list);
        node.opcode().init(Opcodes.PUTFIELD);
        node.name().init(fieldName);
        node.desc().init(desc);
        node.owner().init(owner);
        list.addCopy(node);
    }

    public void RETURN() {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.RETURN);
        list.addCopy(node);
    }
    public void IRETURN() {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.IRETURN);
        list.addCopy(node);
    }
    public void DRETURN() {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.DRETURN);
        list.addCopy(node);
    }
    public void FRETURN() {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.FRETURN);
        list.addCopy(node);
    }
    public void LRETURN() {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.LRETURN);
        list.addCopy(node);
    }
    public void ARETURN() {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.ARETURN);
        list.addCopy(node);
    }

    public void ARRAY_LENGTH() {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.ARRAYLENGTH);
        list.addCopy(node);
    }

    public void NEW_ARRAY(int arrayType) {
        AsmrIntInsnNode node = new AsmrIntInsnNode(list);
        node.opcode().init(Opcodes.NEWARRAY);
        node.operand().init(arrayType);
        list.addCopy(node);
    }

    public void ANEW_ARRAY(String internalName) {
        AsmrTypeInsnNode node = new AsmrTypeInsnNode(list);
        node.opcode().init(Opcodes.ANEWARRAY);
        node.desc().init(internalName);
        list.addCopy(node);
    }
    public void CHECKCAST(String internalName) {
        AsmrTypeInsnNode node = new AsmrTypeInsnNode(list);
        node.opcode().init(Opcodes.CHECKCAST);
        node.desc().init(internalName);
        list.addCopy(node);
    }
    public void INSTANCEOF(String internalName) {
        AsmrTypeInsnNode node = new AsmrTypeInsnNode(list);
        node.opcode().init(Opcodes.INSTANCEOF);
        node.desc().init(internalName);
        list.addCopy(node);
    }
    public void NEW(String internalName) {
        AsmrTypeInsnNode node = new AsmrTypeInsnNode(list);
        node.opcode().init(Opcodes.NEW);
        node.desc().init(internalName);
        list.addCopy(node);
    }

    public void NOP() {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.NOP);
        list.addCopy(node);
    }

    public void IADD() {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.IADD);
        list.addCopy(node);
    }
    public void ISUB() {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.ISUB);
        list.addCopy(node);
    }
    public void IMUL() {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.IMUL);
        list.addCopy(node);
    }
    public void IDIV() {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.IDIV);
        list.addCopy(node);
    }
    public void POP() {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.POP);
        list.addCopy(node);
    }
    public void SWAP() {
        AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode(list);
        node.opcode().init(Opcodes.SWAP);
        list.addCopy(node);
    }
    public void LDC(String s) {
        AsmrLdcInsnNode node = new AsmrLdcInsnNode(list);
        node.opcode().init(Opcodes.LDC);
        // TODO
    }
    public void LDC(Integer s) {
        AsmrLdcInsnNode node = new AsmrLdcInsnNode(list);
        node.opcode().init(Opcodes.LDC);
        // TODO
    }
    public void LDC(Long s) {
        AsmrLdcInsnNode node = new AsmrLdcInsnNode(list);
        node.opcode().init(Opcodes.LDC);
        // TODO
    }
    public void LDC(Double s) {
        AsmrLdcInsnNode node = new AsmrLdcInsnNode(list);
        node.opcode().init(Opcodes.LDC);
        // TODO
    }
    public void LDC(Float s) {
        AsmrLdcInsnNode node = new AsmrLdcInsnNode(list);
        node.opcode().init(Opcodes.LDC);
        // TODO
    }
    public void INVOKE_VIRTUAL(String internalName, String name, String desc) {
        AsmrFieldInsnNode node = new AsmrFieldInsnNode(list);
        node.opcode().init(Opcodes.INVOKEVIRTUAL);
        node.name().init(name);
        node.desc().init(desc);
        node.owner().init(internalName);
        list.addCopy(node);
    }
    public void INVOKE_STATIC(String internalName, String name, String desc) {
        AsmrFieldInsnNode node = new AsmrFieldInsnNode(list);
        node.opcode().init(Opcodes.INVOKESTATIC);
        node.name().init(name);
        node.desc().init(desc);
        node.owner().init(internalName);
        list.addCopy(node);
    }
    public void INVOKE_SPECIAL(String internalName, String name, String desc) {
        AsmrFieldInsnNode node = new AsmrFieldInsnNode(list);
        node.opcode().init(Opcodes.INVOKESPECIAL);
        node.name().init(name);
        node.desc().init(desc);
        node.owner().init(internalName);
        list.addCopy(node);
    }
    public void INVOKE_INTERFACE(String internalName, String name, String desc) {
        AsmrFieldInsnNode node = new AsmrFieldInsnNode(list);
        node.opcode().init(Opcodes.INVOKEINTERFACE);
        node.name().init(name);
        node.desc().init(desc);
        node.owner().init(internalName);
        list.addCopy(node);
    }
}
