# Opcodes
* All jvm opcodes
* `define` - defines macros
  - format `define VariableName text CotentToReplaceTheVariableWith`
  - format `define VariableName argLookup CertifiedClassName`
* `class` - define classes you wish to transform
  - format `class CertifiedClassName`
* `method` - use for matching methods targets
  - format - `method owner methodName desc`
* `match` - encompass a block of bytecode to match to
  - format `match start`
  - format `match end`
    - children:
      - `ordinal`
      - `posistion` - before/after
* `body` - the body of bytecode to be injected
  - format `body bytecode`
  - children
    - the bytecode